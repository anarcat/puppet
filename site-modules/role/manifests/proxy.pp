# a proxy sits in the clouds and acts as a gatekeeper and lighthouse
# for the rest of my infrastructure
class role::proxy {
  include profile::bind::authoritative
  include nftables
  include nftables::rules::out::all
  include profile::nftables::rules::ssh
  include nftables::rules::dns
  include nftables::rules::icmp
}
