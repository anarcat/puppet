# a simple webserver
class role::server::web {
  package { [
    # bandwidth monitor, see also slurm below
    'bmon',
    'certbot',
    'goaccess',
    # network stats
    'iptraf-ng',
    # lightweight graphing
    'nethogs',
    # network usage check, similar to bmon, see
    # https://anarc.at/blog/2018-12-21-report/
    # "bmon has packets per second graphs, while slurm only has
    # bandwidth graphs, but also notices maximal burst speeds which is
    # very useful"
    #
    # nmon is similar (and more)
    'slurm',
  ]:
    ensure => present,
  }
  # adhoc monitoring tool
  package { 'netdata':
    ensure => purged,
  }
  include profile::goatcounter
}
