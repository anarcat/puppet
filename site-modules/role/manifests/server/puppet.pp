# the puppet server, previously known as the "puppetmaster"
class role::server::puppet {
  # configure the agent as well, which happens to enable the "puppet"
  # module which also happens to configure this server, at least
  # partly
  include profile::puppet
  include profile::puppetboard
  include profile::grafana
  # play dumb for now:
  package { 'puppetserver':
    ensure => present,
  }
  # TODO: use the trocla class, when we have fixed trocla in Debian
  # (uploaded 0.3.0). In the meantime we could also do:
  #
  # class { 'trocla::master':
  #  provider => 'gem',
  # }
  package { 'trocla':
    ensure => present,
  }
  file { '/etc/troclarc.yaml':
    content => @(EOF),
    ---
    store_options:
      adapter: :YAML
      adapter_options:
        :file: '/etc/puppet/trocla-secrets.yaml'
    | EOF
  }
  # TODO: configure role account and post hooks
  package { 'g10k':
    ensure => present,
  }
  # server-side lint tools
  package { ['puppet-lint', 'r10k']:
    ensure => present,
  }

  package { 'puppetdb':
    ensure  => present,
    require => Package['postgresql'],
  }
  package { 'postgresql':
    ensure => present,
  }
  # note that some more configuration is done in profile::puppet, as
  # that configures the puppet agent and therefore puppet.conf which
  # is (confusingly) also used by the puppetmaster
  puppet::config::master {
    # TODO: configure the puppetmaster properly
    'environmentpath':      value => '/etc/puppet/code/';
    'storeconfigs':         value => 'true';
    'storeconfigs_backend': value => 'puppetdb';
    'node_terminus':        value => 'exec';
    'external_nodes':       value => '/etc/puppet/puppet-hiera-enc/enc';
  }
  # The above kind of sucks, to be honest. The foreman/puppet
  # configuration is just all *over* the place and adds
  # master-specific settings into main and vice versa. it's kind of
  # horrible:
  # +[main]
  # -ssldir = /var/lib/puppet/ssl
  # +    certname = marcos.anarc.at
  # +    codedir = /etc/puppet/code
  # +    dns_alt_names = puppet,puppet.anarc.at,puppet.orangeseeds.org,marcos.anarc.at,marcos.orangeseeds.org
  # +    hostprivkey = $privatekeydir/$certname.pem { mode = 640 }
  # +    logdir = /var/log/puppet
  # +    pluginfactsource = puppet:///pluginfacts
  # +    pluginsource = puppet:///plugins
  # +    privatekeydir = $ssldir/private_keys { group = service }
  # +    rundir = /var/run/puppet
  # +    server = puppet
  # +    show_diff = false
  # +    ssldir = /var/lib/puppet/ssl
  # +    vardir = /var/lib/puppet
  #  
  # -[master]
  # -vardir = /var/lib/puppet
  # -cadir  = /var/lib/puppet/ssl/ca
  # -dns_alt_names = puppet,puppet.anarc.at,puppet.orangeseeds.org,marcos.anarc.at,marcos.orangeseeds.org
  # -environmentpath=/etc/puppet/code/
  # +[agent]
  # +    classfile = $statedir/classes.txt
  # +    default_schedules = false
  # +    environment = production
  # +    localconfig = $vardir/localconfig
  # +    masterport = 8140
  # +    noop = false
  # +    report = true
  # +    runinterval = 1800
  # +    splay = false
  # +    splaylimit = 1800
  # +    usecacheonfailure = true
}
