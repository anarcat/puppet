# a "shell server"
#
# TODO: this is really just marcos under another name, kind of getting
# into a mess here...
class role::server::shell {
  include profile::irc_bouncer
  class { 'profile::gnupg':
    gui => false,
  }
  include profile::email::server
  package { [
    'bsdgames', # all sorts of stuff, including games
    'feed2exec', # RSS feed reader and automation
    'inetutils-talk', # nostalgia
    'linkchecker', # used by feed2exec to check links
    'myrepos', # keep a bunch of repos in sync
    'nethack-console', # game
    'toot', # to post mastodon toots typically from feed2exec
    'zsh', # extra shell for nerds
  ]:
    ensure => present,
  }
}
