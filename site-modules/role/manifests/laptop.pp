# my laptop(s)
#
# really just a workstation for now, but could diverge for power
# saving, battery stuff and what not
class role::laptop {
  include role::workstation
  package { [
    'battery-stats', # to track battery life history
    'intel-media-va-driver-non-free', # fixes crashes
    'macchanger', # for some anonymity
    'thermald', # tweak power usage according to sensors
    'tlp', # for tlp -b, mainly
  ]:
    ensure => present,
  }
  file { '/lib/systemd/system-sleep/tlp-stat-battery':
    mode    => '0555',
    content => @(EOF),
    #!/bin/sh

    # tlp-stat battery dump hook
    #
    # Copyright (c) 2022 Antoine Beaupré <anarcat@debian.org>
    # This software is licensed under the GPL v2 or later.
    case $1 in
        pre|post)  tlp-stat -b ;;
    esac
    |EOF
  }
  service { 'tlp':
    enable => true,
  }
  file { '/etc/systemd/logind.conf.d/':
    ensure  => directory,
    purge   => true,
    recurse => true,
    force   => true,
  }
  class { 'systemd':
    manage_logind => true,
  }
  file { '/etc/systemd/logind.conf.d/power-suspends.conf':
    content => @(EOF),
    # the power key is too destructive for laptops, just suspend
    # instead, which is probably what we wanted to do here
    [Login]
    HandlePowerKey=suspend
    HandlePowerKeyLongPress=poweroff
    | EOF
    notify  => Service['systemd-logind'],
  }
  file { '/etc/X11/xorg.conf.d/backlight.conf':
    content => @(EOF),
    Section "Device"
        Identifier  "Card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
    EndSection
    | EOF
  }
}
