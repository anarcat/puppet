# my laptop
class role::workstation {
  include nftables
  include nftables::rules::out::all
  #include nftables::rules::ssh
  include nftables::rules::icmp

  nftables::rule { 'default_in-syncthing':
      content => 'tcp dport 22000 accept',
  }
  include profile::nftables::avahi

  include profile::author
  include profile::audio
  include profile::devel
  include profile::etckeeper
  include profile::email::client
  include profile::chat_client
  include profile::desktop
  include profile::games
  include profile::gis
  include profile::graphics
  include profile::radio
  include profile::sysadmin


  package { 'libsasl2-modules': } # required to post mail to submit.tpo

  include profile::postfix::satellite
}
