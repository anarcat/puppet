# configure a sysctl setting in /etc/sysctl.d/ and add it to the runtime
#
# Note that $ensure=false might not work as you'd expect: if you set a
# parameter on a host and then remove it, the runtime parameter will
# still be set, as there is no way for Puppet to know about the
# previous parameter right now.
#
# The duritong/sysctl module does not have that problem, but it writes
# directly to /etc/sysctl.conf, which we dislike.
define sysctl::config(
  String $key,
  String $value,
  Optional[Enum['present','absent']] $ensure = present,
  Optional[String] $comment = '',
) {
  file { "/etc/sysctl.d/${name}.conf":
    ensure  => $ensure,
    content => @("EOF"),
    # file managed by Puppet, do not edit
    #
    # ${comment}
    ${key}=${value}
    |EOF
  }
  exec { "/sbin/sysctl -w ${key}=${value}":
    refreshonly => true,
    subscribe   => File["/etc/sysctl.d/${name}.conf"],
  }
}
