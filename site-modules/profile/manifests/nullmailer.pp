# nullmailer setup for dumb mail remotes
class profile::nullmailer {
  package { 'nullmailer':
    ensure => present,
  }
}
