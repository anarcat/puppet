# configure a local resolver
#
# We use systemd-resolved here because of this article:
#
# https://tailscale.com/blog/sisyphean-dns-client-linux/
#
# We enable:
#
# * DNSSEC - torproject.org and debian.org have DNSSEC and it's nice
#   to be able to trust DNS a little more
#
# * caching - it's disabled by default in the module, but we enable it
#   here because it might help performance. we keep it from caching
#   negative results, however, because that's too annoying (e.g. when
#   creating a new domain and checking it too early)
#
# * use_stub_resolver - use 127.0.0.1:53 as the nameserver in resolv.conf
#
# * dnsovertls - "opportunistic" maye just work?
#
# * domains - we set our domain name explicitely here
#
# We do not enable:
#
# * LLMNR and MulticastDNS - it's unclear why we'd need this nor what
#   the difference is between the two.

# * $dns (upstream DNS servers) - we do our DNS queries directly and
#   do not trust upstream resolvers, since they may be stripping (or,
#   more accurately, failing to do) DNSSEC
class profile::network(
  Boolean $dhcp = true,
  Optional[String] $dns = undef,
) {
  class { 'systemd':
    manage_networkd   => true,
    manage_resolved   => true,
    dns               => $dns,
    # until we have a reliable upstream DNSSEC server
    dnssec            => 'allow-downgrade',
    cache             => 'no-negative',
    # listen on UDP localhost:53 (module default: no, upstream default: TCP/UDP)
    dns_stub_listener => 'udp',
    # adds localhost to resolv.conf, basically
    #
    # in practice, this points /etc/resolv.conf at a file which does
    # that
    use_stub_resolver => true,
    dnsovertls        => 'opportunistic',
    domains           => 'anarc.at',
  }
  if $dhcp {
    systemd::network { '80-dhcp.network':
      content => @(EOF)
      [Match]
      Name=en*

      [Network]
      DHCP=yes
      | EOF
    }
  }
}
