# basic configuration on top of the forge nginx module
class profile::nginx {
  # a lot of that comes from the Torproject.org profile::nginx class,
  # which in turn tries to harmonize the puppet-nginx defaults with
  # Debian's and upstream. See:
  #
  # https://github.com/voxpupuli/puppet-nginx/issues/1359
  class { 'nginx':
    confd_purge           => true,
    server_purge          => true,
    manage_repo           => false,
    # HTTP/2 rocks
    http2                 => 'on',
    # Silence our identity
    server_tokens         => 'off',
    # no need for the whole thing
    package_flavor        => 'light',
    # off since 1.11, not necessary w/ EPOLLEXCLUSIVE
    accept_mutex          => 'off',
    # useful optimizations
    http_tcp_nopush       => 'on',
    gzip                  => 'on',
    # keep low (Debian default) to reduce DOS attack surface (Puppet
    # default is 10m)
    client_max_body_size  => '1m',
    # https://github.com/voxpupuli/puppet-nginx/issues/1372#issuecomment-952791412
    proxy_temp_path       => undef,
    client_body_temp_path => undef,

    # those default to 90s upstream, and are absent in Debian. Below
    # are the Puppet module default, but who cares?
    #proxy_connect_timeout => '60s',
    #proxy_read_timeout    => '60s',
    #proxy_send_timeout    => '60s',

    # those would be useful when we setup a cache
    #proxy_cache_path      => '/var/cache/nginx/',
    #proxy_cache_levels    => '1:2',
    #proxy_cache_keys_zone => 'default:10m',
    ## XXX: hardcoded, should just let nginx figure it out
    #proxy_cache_max_size  => '15g',
    #proxy_cache_inactive  => '24h',

    # default puppet module doesn't include TLSv1.3
    #
    # we include 1.1 for old time's sake, and because this is a
    # non-critical server, but more security-sensitive ones should
    # default to 1.2+ only
    ssl_protocols         => 'TLSv1 TLSv1.1 TLSv1.2 TLSv1.3',
    # ssl_ciphers could be reviewed as well, see https://trac.torproject.org/projects/tor/ticket/32351
  }
  include nftables::rules::http
  include nftables::rules::https
}
