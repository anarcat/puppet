# free software games I play
class profile::games {
  package { [
    'endless-sky',
    'freeorion',
  ]:
    ensure => present,
  }
}
