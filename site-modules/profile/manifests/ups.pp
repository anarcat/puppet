# manage a UPS attached to this server
#
# alternative nut-server and nut-client
#
# doesn't have email notifications by default, but config is simple:
#
# echo MODE=standalone >> /etc/nut/nut.conf
# nut-scanner | sed 's/nutdev1/apc/' >> /etc/nut/ups.conf
# service nut-server restart
#
# then `upsc apc@localhost` gives info.
#
# possible alternative https://www.phind.com/search?cache=ak31rai8uf1cjecpxh76ixqx
# https://github.com/DRuggeri/nut_exporter
#
# implementation of an email notifier
# https://github.com/networkupstools/nut/blob/3f4be327048927ead2fcc06a88c1d1ae171f60a0/docs/scheduling.txt#L37
class profile::ups {
  package { 'apcupsd':
    ensure => installed,
  }
  file_line { 'apcupsd.conf-no-net':
    path  => '/etc/apcupsd/apcupsd.conf',
    line  => 'NETSERVER off',
    match => '^NETSERVER .*',
  }
  file_line { 'apcupsd.conf-no-serial':
    ensure            => absent,
    path              => '/etc/apcupsd/apcupsd.conf',
    match             => '^DEVICE .*',
    match_for_absence => true,
  }
}
