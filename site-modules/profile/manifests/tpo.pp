# packages and tools useful for my work in TPA/TPO
class profile::tpo {
  package { [
    'python3-ldap', # for fabric_tpa stuff
  ]:
    ensure => installed,
  }
}
