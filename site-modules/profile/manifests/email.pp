# code common to client and server
class profile::email {
  include profile::gnupg
  ensure_packages([
    # so that we have a notmuch client without depending on emacs
    'afew',
    'bsd-mailx',
    'neomutt',
    'notmuch',
    'python3-notmuch',
  ])
  # not using those anymore
  package { [
    'mutt',
    'offlineimap',
    'syncmaildir',
  ]:
    ensure => 'purged',
  }
}
