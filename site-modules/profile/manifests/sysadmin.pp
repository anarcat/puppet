# sysadmin-y tools I use regularly
#
# TODO: not limited to server-side tools, but should be
class profile::sysadmin {
  ensure_packages([
    'analog', # log file analyser
    'ansible', # config management, rarely used
    'ansible-lint', # linter for the above
    'ansible-mitogen', # improve performance
    'apache2-utils', # for ab2
    'at', # run shell scripts in the future
    'borgbackup-doc',
    'bpftrace', # swiss-army debugging knife, used execsnoop
    'cu', # for talking to serial lines, used by screen
    'cumin', # batch jobs on multiple servers
    #'curl', # in base
    #'dateutils', # commandline date operations: add, diff, etc, in devel
    'debian-goodies', # all sorts of debian stuff
    'dmarc-cat', # (poor) DMARC reports parser
    'dstat', # important sysadmin diagnostic tool
    'f3', # flash cards checking
    'fio', # disk benchmarks
    'gddrescue', # recover data
    'gdu', # like ncdu but faster, keeping ncdu because https://github.com/dundee/gdu/issues/202
    'geoip-bin', # IP address lookups
    #'git', # in base
    'gparted',
    'hashdeep', # compare file lists based on checksums
    'hdparm',
    'htop', # basic live monitoring
    'hwinfo',
    'i7z', # information about intel CPUs
    'intel-microcode', # intel microcode updates
    'internetarchive', # to throw things at archive.org
    'ioping', # I/O stats
    'iotop-c', # "who eats the I/O"
    'ipcalc', 'ipv6calc', # IP address calculators
    'iperf3', # network benchmarks
    'ipmitool', # talk with remote management interfaces
    'iputils-ping', # venerable ping, without suid
    'libbpf-tools', # execsnoop and many, many others
    'libnss3-tools', # another X509 lib TODO: why?
    'lshw', # basic hardware inventory (replaces default "discover" from d-i)
    'lsof', # list open files, see also netstat or ss
    'minisign', # alternative to OpenPGP, used to sign CalyxOS releases
    'moreutils', # bunch of stuff
    'mosh', # like SSH
    'ncdu', # drill down per-folder disk usage, dupe of gdu because https://github.com/dundee/gdu/issues/202
    'nmap', # network probing
    'nvme-cli', # check NVMe drives stats
    'nwipe', # wipe drives securely
    #'numutils', # TODO: wtf?
    'passwdqc', # password strenght checks and nicer generator
    'pciutils', # lspci
    'powertop', # power usage optimization tool
    'powerstat', # power usage measurement tool, see https://anarc.at/hardware/laptop/framework-12th-gen/
    'progress', # monitor long transfers
    'pv', # magic progress bar, used all the time
    'python3-clustershell', # cumin dependency, see Debian bug #924685
    'rclone', # swiss-army knife of remote APIs, best s3 client
    'rename', # easier batch renames
    'reptyr', # reparent process
    'ripgrep', # just too fast to live without nowadays, minimal (runtime) deps
    'rt4-clients', # to talk to RT (Request Tracker) instances
    'sdparm',
    'siege', # benchmark
    'smartmontools', # check disks everywhere
    'smem', # per process memory usage, see also smemstat
    'sshfs', # nice hack
    'ssss', # split secrets
    'strace',
    'testssl.sh', # debug TLS connexions
    'tftp-hpa', # incredibly, I still need TFTP from time to time
    'time', # improved built-in time with resource usage
    'tor',
    'ttyrec', # check with asciinema
    #'wireshark', # TODO: move to xorg?
    #'yubikey-personalization', # TODO: move to xorg?
  ])
  package { [
    'apt-transport-https', # transitionnal package
    'asciinema', # recording sessions, rarely used
    'bup', # old backup tool
    'canid', # never used
    'duff', # dupes checker
    'geoip-database-extra', # removed from Debian some time after buster, non-free now anways
    'hopenpgp-tools', # openpgp tools, failed to get into bookworm, partly replaced by sq
    'openstack-clients', # manage OpenStack clusters and VMs, poorly, RC bug: #1093157
    'pwgen', # considered insecure
    'px', # nicer ps? never really used
    'rcs', # ad-hoc VCS, git is enough
    'rmlint', # dupe file checker, also consider duff, hardlink, rdfind, jdupe, rmlint
    'stressant', # really useful on burn-in, so not on a final machine
    'sysstat', # annoying cronjobs, iostat is not worth it
    'undistract-me', # buggy, replaced with a simple bell
  ]:
    ensure => 'purged',
  }
}
