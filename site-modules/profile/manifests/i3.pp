# configure a basic xorg environment
class profile::i3 (
  Enum['present','absent'] $ensure = 'present',
) {
  package { [
    'arandr',
    'autorandr', # automatically configure monitor outputs
    'compton', # for 3d perf on the framework, see also i3
    'dbus-x11', # for dbus-launch, used in my custom x11 session
    'fim',
    'i3', # see also compton
    'lightdm',
    'maim',
    'py3status',
    'rofi',
    'slop',
    'rxvt-unicode',
    'xbacklight',
    'xdotool',
    'xinput',
    'xkbset',
    'xprintidle',
    'xscreensaver', # we still install this so that xsecurelock can use the "hacks"
    'xscreensaver-screensaver-bsod',
    'xsecurelock',
    'xsel',
    'xss-lock',
    'xterm',
  ]: ensure => $ensure,
  }
  file { '/etc/systemd/user/xscreensaver.service':
    # now using xss-lock
    ensure  => absent,
    # bits from https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=854691
    # the Restart=on-failure is mine
    content => @(EOF)
    [Unit]
    Description=XScreenSaver
    Documentation=man:xscreensaver(1)
    Documentation=man:xscreensaver-command(1)
    Documentation=man:xscreensaver-demo(1)
    PartOf=graphical-session.target

    [Service]
    ExecStart=/usr/bin/xscreensaver
    Restart=on-failure

    [Install]
    WantedBy=default.target
    | EOF
  }
  if ($ensure == 'present') {
    # TODO: alternatives module?
    file { '/etc/alternatives/x-terminal-emulator':
      ensure => link,
      target => '/usr/bin/xterm',
    }
  }
}
