# language server protocol packages
#
# Those are mostly useful for development workstations. They are not
# part of the editor class (typically profile::emacs) because they are
# actually editor-agnostic and could be reused by others.
class profile::lsp {
  package { [
    'python3-pylsp',
    'python3-pylsp-black',
    'python3-pylsp-mypy',
    'python3-pluggy', # required for newer pylsp
  ]: }
  # conflicts with black
  package { 'python3-pylsp-isort':
    ensure => 'purged',
  }

  # removed from Debian (https://bugs.debian.org/1009941),
  # functionality present in python3-pylsqp anyways
  package { 'python3-pylsp-flake8':
    ensure => 'purged',
  }

  package { 'gopls': }
}
