# setup a hook to restore clock on power failures
#
# this is typically necessary when the CMOS battery is bad, or on some
# embedded system without an onboard RTC (Real-Time Clock).
#
# See https://anarc.at/blog/2021-03-22-email-crash/
class profile::fake_hwclock {
  package { 'fake-hwclock': }
}
