# GPS and GIS tools
class profile::gis {
  package { [
    'gpsbabel',
    'josm',
    'qmapshack',
    'stellarium',
  ]:
    ensure => present,
  }
  package { [
    'gpsd', # not really used that often
  ]:
    ensure => purged,
  }
}
