# network diagnostics tools
#
# This was split out of profile::sysadmin to have a smaller set of
# packages which can (and will) be installed everywhere.
class profile::base::network {
  ensure_packages([
    'bind9-dnsutils', # dig, nslookup, nsupdate
    'bind9-host', # host command
    'curl', # used all the time
    'ethtool', # link negociation and speed
    'fping', # batch ping jobs
    'iftop', # bandwidth information
    'mtr-tiny', # nice traceroute, without X
    'netcat-openbsd', # the new netcat
    'netcat-traditional', # the old netcat
    'oping', # nicer ping
    'rsync', # can't live without file transfers, scp deprecated
    'sipcalc', # ip address calculator
    'socat', # netcat on steroids
    'swaks', # email diagnostics
    'tcpdump',
    'vnstat', # count packets, important for laptop, useful everywhere
    'wget', # like curl, but not quite, consider replacing
    'whois',
  ])
}
