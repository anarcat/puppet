# Sway Wayland compositor configuration
class profile::sway {
  include profile::pipewire
  package { [
    'brightnessctl', # screen dimming, xbacklight replacement
    'foot', # xterm replacement
    'foot-extra-terminfo', # for tmux truecolor, see Debian #1090821
    'fuzzel', # selector, rofi replacement
    'gammastep', # shift color temperature to reduce blue light, redshift replacement
    'gdm3', # TODO: resolve conflict with lightdm
    'grim', 'slurp', # screenshots, maim/slurp replacement
    'kanshi', # output/monitor autodetection
    'mako-notifier', # notifications
    'nwg-displays', # monitor configuration, arandr/wdisplays replacement
    'poweralertd', # for charge/discharge notifications
    'sway', # i3 replacement
    'swayidle', # xautolock/xscreensaver replacement
    'swaylock', # lock screen, xsecurelock/xscreensaver replacement
    'swayosd', # on-screen notification for brightness, volume controls
    'waybar', # status bar, py3status replacement
    'wev', # show events, xev replacement
    'wf-recorder', # screen recorder, kooha a possible replacement
    'wl-clipboard', # clipboard pipes
    'wlr-randr', # xrandr replacement
    'wtype', # xdotool replacemenet
    'xdg-desktop-portal-wlr', # for screensharing
  ]:
    ensure => installed,
  }
  package { [
    'dunst', # replaced with mako
    'sway-notification-center', # replaced with mako
    'wdisplays', # replaced with nwg-displays
  ]:
    ensure => purged,
  }
  file { '/usr/bin/sway-user-service':
    source => 'puppet:///modules/profile/sway/sway-user-service',
    mode   => '0555',
  }
  file { '/usr/share/wayland-sessions/sway-session.desktop':
    source => 'puppet:///modules/profile/sway/sway-session.desktop',
  }
}
