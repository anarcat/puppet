# configuration common to ZFS servers
class profile::zfs(
  Boolean $manage_sanoid = true,
  Optional[Array[String[1]]] $pools = ['rpoolssd', 'bpoolssd'],
  Optional[String[1]] $sanoid_extra_cfg = undef,
) {
  # needed for zed monitoring alerts
  ensure_packages(['bsd-mailx'])
  # TODO: export dropbear SSH keys
  #
  # TODO: configure authorized_keys file, which is basically:
  #
  # sed -e '/^ssh-/ s#^#no-port-forwarding,no-agent-forwarding,no-X11-forwarding #' "$AUTHKEYS_SOURCE" > "$MNTPOINT/etc/dropbear-initramfs/authorized_keys"
  # update-initramfs -u
  #
  # taken from tsa-misc.git,
  # installer/post-scripts/50-tor-install-luks-setup, except that we
  # don't hardcode the `command` because we actually should be calling
  # zfsunlock
  package { ['zfs-dkms', 'zfs-initramfs', 'dropbear-initramfs']:
    ensure => 'present',
  }
  if $manage_sanoid {
    package { 'sanoid':
      ensure => installed,
    }
    file { '/etc/sanoid/sanoid.conf':
      content => epp('profile/sanoid.conf', {
        pools => $pools,
        extra => $sanoid_extra_cfg,
      } ),
      require => File['/etc/sanoid'],
    }
    file { '/etc/sanoid':
      ensure => directory,
    }
    systemd::unit_file { 'sanoid.service':
      ensure  => present,
      path    => '/lib/systemd/system/',
      enable  => true,
      active  => false,
      require => Package['sanoid'],
    }
  }
}
