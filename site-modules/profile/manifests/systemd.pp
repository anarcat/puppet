# global systemd configuration tweaks
#
# some configuration also resides in hiera
class profile::systemd {
  include systemd

  file { '/etc/systemd/journald.conf.d/2w-retention.conf':
    content => @(EOF),
    [Journal]
    # keep at most two weeks of log
    #
    # default is to keep logs indefinitely up to a certain percentage of disk space
    # or max 4GB
    MaxRetentionSec=2week
    # rotate logfiles daily instead of monthly since the latter makes no sense anymore
    MaxFileSec=1day
    | EOF
  }
}
