# install goatcounter in a docker container
class profile::goatcounter {
  include profile::docker
  file { '/etc/docker/goatcounter':
    ensure => directory,
  }
  -> file { '/etc/docker/goatcounter/docker-compose.yml':
    ensure => present,
    source => 'puppet:///modules/profile/compose-goatcounter.yml',
  }
  # this will start the container(s)
  -> docker_compose { 'goatcounter':
    ensure        => present,
    compose_files => ['/etc/docker/goatcounter/docker-compose.yml'],
  }
}
