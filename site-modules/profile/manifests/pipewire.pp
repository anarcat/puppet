# configure pipewire with compatibility with Jack and Pulseaudio
#
# based on https://wiki.debian.org/PipeWire?action=recall&rev=9
#
# TODO:
#
# The following still needs to be ran as a regular user:
#
# # Check for new service files with:
# systemctl --user daemon-reload
# # Disable and stop the PulseAudio service with:
# systemctl --user --now disable pulseaudio.service pulseaudio.socket
# # Enable and start the new pipewire-pulse service with:
# systemctl --user --now enable pipewire pipewire-pulse
#
# # and possibly disable the pulseaudio service from starting:
# systemctl --user mask pulseaudio
#
# it's unclear how to do this from Puppet meaningfully without
# starting to manage user-level configurations (which we have
# avoided so far).  so it's irrelevant for this puppet module for
# now.
class profile::pipewire(
  Enum['present', 'absent'] $ensure = 'present',
) {
  package { [
    'pipewire',
    'pipewire-alsa',
    'pipewire-jack',
    'pipewire-pulse',
    'qjackctl',
    'wireplumber',
  ]:
    ensure => $ensure,
  }
  # JACK compatibility
  file { "/etc/ld.so.conf.d/pipewire-jack-${facts['ruby']['platform']}.conf":
    ensure => $ensure,
    target => "/usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-${facts['ruby']['platform']}.conf",
    notify => Exec['ldconfig'],
  }
  exec { 'ldconfig':
    command     => '/sbin/ldconfig',
    refreshonly => true,
  }
}
