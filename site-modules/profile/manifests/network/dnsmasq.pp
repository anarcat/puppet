# a simple DNS/DHCP server
#
# This configures a forwarding DNS server and DHCP server to listen on
# the local interface ($lan_if).
#
# A part of the configuration (static leases) is secret, in a "files"
# mount, for privacy reasons (MAC addresses).
#
# DNSmasq is *not* a recursive nameserver, we might want to fix that
# and use knot or unbound, but we'd still need a DHCP server, and
# ISC's deprecated theirs. They suggest Kea, but that seem to
# *require* a SQL database which is ridiculous.
#
# There are two modules in the forge for knot at the time of writing,
# and both mostly seem aimed at making knot an authoritative
# nameserver:
#
# https://forge.puppet.com/modules/tobru/knot
# https://forge.puppet.com/modules/icann/knot
#
# What we want instead is knot-resolver, a different package, for
# which there's no puppet module:
#
# https://www.knot-resolver.cz/
#
# There are some issues with knot: riseup saw critical bugs during
# production deployment that were eventually fixed or worked around,
# but we should be careful about it. E.g.
#
# write amplification (workaround: tmpfs):
# https://gitlab.nic.cz/knot/knot-resolver/-/issues/563
#
# "Updating riseup zone gives semantic check failure" (solved with
# zonefile-load setting change)
#
# they also needed to run the upstream apt repo, although maybe that
# is less of an issue since bookworm.
#
# @param dns boolean enable DNS service
# @param dhcp boolean enable DHCP service
# @param wan_if external interface, public network
# @param lan_if internal interface, local network
# @param lan_addr internal IP address
# @param lan_size network size, e.g. "24" for the typical 192.168.0.1/24
# @param dns_server IP address of the DNS server (typically the same,
# without the netmask)
# @param lan_start beginning of the DHCP range
# @param lan_end end of the DHCP range
class profile::network::dnsmasq(
  Boolean $dns = true,
  Boolean $dhcp = true,
  String[1] $wan_if = $profile::network::router::wan_if,
  String[1] $lan_if = $profile::network::router::lan_if,
  Stdlib::IP::Address::V4::Nosubnet $lan_addr = '192.168.0.1',
  # type pattern taken from stdlib's Stdlib::IP::Address::V4::CIDR
  Pattern[/\A([0-9]|[12][0-9]|3[0-2])\z/] $lan_size = '24',
  Stdlib::IP::Address::V4::Nosubnet $dns_server = $profile::network::router::dns_server,
  Stdlib::IP::Address::V4::Nosubnet $lan_start = $profile::network::router::lan_start,
  Stdlib::IP::Address::V4::Nosubnet $lan_end = $profile::network::router::lan_end,
) {
  # create a $lan_cidr with the host bits zero'd out. this turns, for
  # example, 192.168.0.1/24 into 192.168.0.0/24.
  $_lan_net = extlib::cidr_to_network("${lan_addr}/${lan_size}")
  $lan_cidr = "${_lan_net}/${lan_size}"
  class { 'dnsmasq':
    purge_config_dir => true,
  }
  dnsmasq::conf { 'interfaces':
    content => @("EOF")
    interface=${lan_if}
    | EOF
  }
  if $dns {
    dnsmasq::conf { 'dns':
      content => @(EOF),
      # fallback DNS server
      #
      # TODO: seems like resolv.conf doesn't get updated by systemd,
      # need to look into this
      server=9.9.9.9
      server=1.1.1.1
      domain=lan
      local=/lan/
      no-negcache
      # DNSSEC configuration
      dnssec
      # trust anchor is provided by the Debian package, which passes it
      # as an argument on startup.
      #
      # if we have trouble with the clock, uncomment this:
      #dnssec-no-timecheck
      # also consider dnssec-timestamp=<path>

      # only forward FQDN requests
      domain-needed
      # refuse bogus private (RFC6303) reverse lookups
      bogus-priv
      # reject upstream responses in the local range
      stop-dns-rebind
      # except for 127/8 (RBLs)
      rebind-localhost-ok
      # only answer local machines, failsafe
      local-service

      # RFC6761 included configuration file for dnsmasq
      #
      # includes a list of domains that should not be forwarded to Internet name servers
      # to reduce burden on them, asking questions that they won't know the answer to.

      server=/bind/
      server=/invalid/
      server=/local/
      server=/localhost/
      server=/onion/
      server=/test/

      # hardcoded server replies
      #
      # point all airgradient sensors to local prometheus exporter
      address=/hw.airgradient.com/45.72.186.94
      # use /foo.example.com/ for NXDOMAIN and /foo.example.com/# for
      # "null" (0.0.0.0 AKA localhost for some clients)
      | EOF
    }
  } else {
    dnsmasq::conf { 'dns':
      content => "# disable DNS resolution\nport=0\n",
    }
  }
  if $dhcp {
    $lan_mask = extlib::cidr_to_netmask($lan_cidr)
    dnsmasq::conf { 'dhcp':
      content => @("EOF"),
      # disabled on WAN, naturally
      no-dhcp-interface=${wan_if}
      dhcp-range=set:lan,${lan_start},${lan_end},${lan_mask},12h
      dhcp-option=option:dns-server,${dns_server}
      dhcp-authoritative
      # cover for some particular clients
      dhcp-broadcast=tag:needs-broadcast

      dhcp-ignore-names=tag:dhcp_bogus_hostname
      # vendored from OpenWRT:
      # dhcpbogushostname.conf included configuration file for dnsmasq
      #
      # includes a list of hostnames that should not be associated with dhcp leases
      # in response to CERT VU#598349
      # file included by default, option dhcpbogushostname 0  to disable
      dhcp-name-match=set:dhcp_bogus_hostname,localhost
      dhcp-name-match=set:dhcp_bogus_hostname,wpad
      | EOF
    }
    dnsmasq::conf { 'ethers':
      source => 'puppet:///files/dnsmasq.ethers',  # lint:ignore:puppet_url_without_modules
    }
  }
  # by default, DNSmasq doesn't do DHCP, unless dhcp-range is defined
}
