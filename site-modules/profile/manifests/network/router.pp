# a home router
#
# this provides NAT to IPv4-starved users, along with a DMZ host where
# all traffic is forwarded and a bunch of other forwards.
#
# It also provides DHCP and DNS service through
# profile::network::dnsmasq.
#
# @param wan_if external interface, public network
# @param lan_if internal interface, local network
# @param wan_addr publicly available IP address
# @param lan_addr internal IP address
# @param lan_size network size, e.g. "24" for the typical 192.168.0.1/24
# @param lan_start beginning of the DHCP range
# @param lan_end end of the DHCP range
# @param dns_server IP address of the DNS server (typically the same
# as lan_addr)
class profile::network::router(
  String[1] $wan_if = 'eth0',
  String[1] $lan_if = 'eth1',
  Stdlib::IP::Address::V4::Nosubnet $wan_addr = $facts['networking']['ip'],
  Stdlib::IP::Address::V4::Nosubnet $lan_addr = '192.168.0.1',
  # type pattern taken from stdlib's Stdlib::IP::Address::V4::CIDR
  Pattern[/\A([0-9]|[12][0-9]|3[0-2])\z/] $lan_size = '24',
  Stdlib::IP::Address::V4::Nosubnet $lan_start = '192.168.0.100',
  Stdlib::IP::Address::V4::Nosubnet $lan_end = '192.168.0.249',
  Stdlib::IP::Address::V4::Nosubnet $dns_server = $lan_addr,
) {
  include profile::network::dnsmasq
  class { 'profile::network':
    dhcp => false,
  }

  # create a $lan_cidr with the host bits zero'd out. this turns, for
  # example, 192.168.0.1/24 into 192.168.0.0/24.
  $_lan_net = extlib::cidr_to_network("${lan_addr}/${lan_size}")
  $lan_cidr = "${_lan_net}/${lan_size}"

  # WAN: DHCP
  systemd::network { 'wan.network':
    content => @("EOF")
    [Match]
    Name=${wan_if}

    [Network]
    DHCP=yes
    | EOF
  }
  # LAN: static
  systemd::network { 'lan.network':
    content => @("EOF")
    [Match]
    Name=${lan_if}

    [Network]
    Address=${lan_addr}/${lan_size}
    | EOF
  }
  package { 'ifupdown':
    ensure => purged,
  }

  sysctl::config { 'ip_forward':
    key     => 'net.ipv4.ip_forward',
    value   => '1',
    comment => 'route traffic between interfaces',
  }
  # https://wiki.nftables.org/wiki-nftables/index.php/Quick_reference-nftables_in_10_minutes
  # inspired by https://wiki.nftables.org/wiki-nftables/index.php/Simple_ruleset_for_a_home_router
  class { 'nftables':
    inet_filter      => true,
    nat              => true,
    in_out_conntrack => true,
    fwd_conntrack    => true,
    # bump log limit, default is 3/minute, which makes it hard to debug
    log_limit        => '60/minute burst 5 packets',
  }
  include nftables::rules::icmp
  class { 'nftables::rules::out::puppet':
    puppetserver => '0.0.0.0/0',
    #puppetserver => '64.18.183.94',  # XXX: hardcoded IP
  }
  nftables::rules::masquerade { $wan_if:
    oif   => $wan_if,
    saddr => $lan_cidr,
  }
  # include nftables::rules::node_exporter
  # generic "allow mostly everything" rules
  nftables::rule {
    'default_in-all': content => "iifname ${lan_if} accept";
    'default_fwd-all': content => "iifname ${lan_if} oifname ${wan_if} accept";
    # those are probably superfluous with the above allow all in
    'default_in-dns_udp': content => "iifname ${lan_if} udp dport 53 accept";
    'default_in-dns_tcp': content => "iifname ${lan_if} tcp dport 53 accept";
    'default_in-dhcpv4': content => "iifname ${lan_if} udp sport {67,68} udp dport {67,68} accept";
    'default_out-dhcpv4': content => "oifname ${lan_if} udp sport {67,68} udp dport {67,68} accept";
  }
  # marcos / bastion host forwarding
  nftables::rule {
    # let DNAT forwards through
    'default_fwd-accept_dnat': content => 'ct status dnat accept';
    # bastion host: forward everything to marcos
    #
    # similar to below's rules::forward, but without target_port
    'PREROUTING-dnat_marcos':
      table   => "ip-${nftables::nat_table_name}",
      order   => '90',
      content => "iifname ${wan_if} dnat to 192.168.0.3";
    # reflection
    'PREROUTING-dnat_marcos_reflect':
      table   => "ip-${nftables::nat_table_name}",
      order   => '92',
      content => "ip saddr ${lan_cidr} ip daddr ${wan_addr}/32 dnat to 192.168.0.3";
    'POSTROUTING-dnat_marcos_reflect':
      table   => "ip-${nftables::nat_table_name}",
      order   => '92',
      content => "ip saddr ${lan_cidr} ip daddr 192.168.0.3/32 snat to ${wan_addr}";
  }

  # port forwards
  profile::nftables::rules::forward {
    default:
      lan_cidr => $lan_cidr,
      lan_addr => $lan_addr,
      wan_if   => $wan_if,
      wan_addr => $wan_addr;
    'micah': target_addr => '192.168.0.44', target_port => 4422, wan_port => 4422;
    'dalresscue02ssh': target_addr => '192.168.0.46', target_port => 22, wan_port => 4622;
    'dalresscue02nrpe': target_addr => '192.168.0.46', target_port => 5666, wan_port => 5666;
    'dalresscue02bacula': target_addr => '192.168.0.46', target_port => 9102, wan_port => 9102;
    'dalresscue02nodexp': target_addr => '192.168.0.46', target_port => 9100, wan_port => 9100;
    'dalresscue02apachexp': target_addr => '192.168.0.46', target_port => 9117, wan_port => 9117;
    'margaret': target_addr => '192.168.0.1', target_port => 22, wan_port => 2222;
  }
}
