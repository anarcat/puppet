# a simple bridge over the given interface
#
# this is mainly to run VMs
#
# note that we do *not* include profile::network here, because *that*
# configures systemd-resolved and it's not able to do DNSSEC properly
# if the upstream nameserver doesn't (and it doesn't).
define profile::network::bridge(
  String $address,
  String $gateway,
  String $netname = 'lan0',
  String $device = 'eno1',
  String $network_extra = '',
) {
  ensure_packages(['bridge-utils'])
  class { 'systemd':
    manage_networkd   => true,
  }
  systemd::network { "${name}.netdev":
    content => @("EOF"),
    # help in systemd.netdev(5)
    [NetDev]
    Name=${name}
    Kind=bridge
    #STP=false
    | EOF
  }
  systemd::network { "${name}.network":
    content => @("EOF"),
    [Match]
    Name=${device}

    [Network]
    Bridge=${name}
    | EOF
  }

  systemd::network { "${netname}.network":
    content => @("EOF"),
    [Match]
    Name=${name}

    [Network]
    #DHCP=ipv4
    # no effect unless systemd-resolved.service is used (and it's currently not)
    DNS=127.0.0.1
    Address=${address}
    Gateway=${gateway}
    ${network_extra}
    | EOF
  }
}
