# incoming is where packages land *before* being rebuilt for unstable
#
# useful to catch new packages fresh out of NEW
class profile::package::incoming {
  file { '/etc/apt/sources.list.d/incoming.list':
    content => @(EOF),
    deb https://incoming.debian.org/debian-buildd buildd-unstable main contrib non-free
    | EOF
  }
  file { '/etc/apt/preferences.d/incoming.pref':
    content => @("EOF"),
    Package: *
    Pin: origin incoming.debian.org
    Pin-Priority: 1
    | EOF
  }
}
