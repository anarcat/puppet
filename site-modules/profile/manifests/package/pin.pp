# pin another distribution but make it available
#
# This is a noop if we are already running that distribution
#
# @param suite the distribution (e.g. "bullseye" or "testing"),
# defaults to $name
define profile::package::pin (
  Enum['present','absent'] $ensure = 'present',
  String[1] $package_name = $name,
  Optional[String] $codename = 'trixie',
  Optional[Integer] $priority = 500,
) {
  # do not create the pin if we are already in the target suite
  if $ensure == 'present' and $facts['os']['distro']['codename'] == $codename {
    $_ensure = 'absent'
  } else {
    $_ensure = $ensure
  }
  file { "/etc/apt/preferences.d/${name}.pref":
    ensure  => $_ensure,
    content => @("EOF"),
    Package: ${package_name}
    Pin: release n=${codename}
    Pin-Priority: ${priority}
    | EOF
  }
  if $codename == 'trixie' {
    include profile::package::testing
  }
}
