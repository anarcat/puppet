# import packages from unstable in stable, but pinned
class profile::package::unstable(
  Enum['present','absent'] $ensure = 'present',
  Integer $pin = 1,
) {
  # TODO: use the apt module instead, because this doesn't work on
  # first run (it might not run before the above and won't run apt-get
  # update, it hardcodes a version number, etc...)
  file { '/etc/apt/sources.list.d/sid.list':
    ensure  => $ensure,
    content => @(EOF),
    deb https://deb.debian.org/debian/ sid main contrib non-free non-free-firmware
    deb-src https://deb.debian.org/debian/ sid main contrib non-free non-free-firmware
    | EOF
  }
  file { '/etc/apt/preferences.d/sid.pref':
    ensure  => $ensure,
    content => @("EOF"),
    Package: *
    Pin: release n=sid
    Pin-Priority: ${pin}
    | EOF
  }
}
