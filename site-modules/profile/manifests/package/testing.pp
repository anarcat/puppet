# import packages from testing in stable, but pinned
class profile::package::testing(
  Enum['present','absent'] $ensure = 'present',
  Boolean $force = false,
  Optional[String] $codename = 'trixie',
  Optional[Integer] $pin = 1,
) {
  # this logic is replicated from profile::package::pin
  #
  # it's kind of weird, but it ensures we remove the sources.list and
  # pin once/if we run testing
  if !$force and $ensure == 'present' and $facts['os']['distro']['codename'] == $codename {
    $_ensure = 'absent'
  } else {
    $_ensure = $ensure
  }
  # TODO: use the apt module instead, because this doesn't work on
  # first run (it might not run before the above and won't run apt-get
  # update, it hardcodes a version number, etc...)
  file { "/etc/apt/sources.list.d/${codename}.list":
    ensure  => $_ensure,
    content => "deb https://deb.debian.org/debian/ ${codename} main contrib non-free non-free-firmware\n",
  }
  file { "/etc/apt/preferences.d/${codename}.pref":
    ensure  => $_ensure,
    content => @("EOF"),
    Package: *
    Pin: release n=${codename}
    Pin-Priority: ${pin}
    | EOF
  }
}
