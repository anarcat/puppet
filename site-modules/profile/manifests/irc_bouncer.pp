# an irc bouncer
class profile::irc_bouncer(
  Enum['present','absent'] $ensure = 'present',
) {
  package { [
    'bitlbee',
    'bitlbee-plugin-mastodon',
    'irssi-scripts',
    'weechat',
    'znc',
  ]:
    ensure => $ensure,
  }
  package { [
    'irssi-plugin-otr',
    'irssi-plugin-xmpp',
  ]:
    ensure => 'purged',
  }
  class { 'dtach::irssi':
    ensure => $ensure,
    user   => 'anarcat-irc',
  }
  sshd::authorized_key_collect { 'chat_clients':
    ensure      => $ensure,
    target_user => 'anarcat-irc',
    collect_tag => 'profile::chat_client',
  }
}
