# allow all inbound
#
# this class is to allow complex hosts to easily transition to a more
# stringent firewall policy by modifying the fallback policy to an
# "allow all". then specific ports can be blocked and allowed until we
# have a good confidence we can remove this class and revert to a
# "default" block pattern which is more desirable.
class profile::nftables::in::all {
  include nftables
  include nftables::rules::out::all
  nftables::rule {
    'default_in-all':
      order   => '90',
      content => 'accept',
  }
}
