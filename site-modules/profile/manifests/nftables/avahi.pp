# allow local DNS discovery
#
# @param saddr list of source network ranges to accept
# @example allow Avahi/mDNS from the local LAN:
#  class { 'profile::nftables::avahi':
#    saddr => ['192.168.0.0/16'],
#  }
class profile::nftables::avahi(
  Array[Stdlib::IP::Address::V4,1] $saddr = ['0.0.0.0/0'],
) {
  nftables::rule { 'default_in-avahi_udp':
    content => "ip saddr { ${saddr.join(', ')} } udp dport 5353 accept";
  }
}
