# @summary allow ssh in only from certin networks
#
# @param allow list of allowed CIDR
# @param ports ssh ports
class profile::nftables::rules::ssh (
  Array[Stdlib::Port,1] $ports = [22],
  Optional[Array[Stdlib::IP::Address::V4::CIDR,1]] $allow = undef,
) {
  if $allow {
    nftables::rule { 'default_in-sshallow':
      content => "ip saddr {${join($allow, ',')}} tcp dport {${join($ports,', ')}} accept",
    }
  } else {
    include nftables::rules::ssh
  }
}
