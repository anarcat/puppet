# forward a service from the outside to internal services
#
# This forwards an external port to an internal machine using DNAT. It
# also sets up reflection so that internal hosts can access those
# services transparently by using the same external IP address.
#
# Example:
#
#     profile::network::forward {
#       default:
#         lan_cidr => '192.168.0.0/24',
#         wan_if   => 'eth0',
#         wan_cidr => '10.0.0.1/32',
#       'http':
#         target_addr => '192.168.0.10',
#         target_port => '80',
#         wan_port    => '8080',
#       'https':
#         target_addr => '192.168.0.10',
#         target_port => '443',
#         wan_port    => '8443',
#     }
#
# The above forwards ports 8080 and 8443 to the machine 192.168.0.10's
# regular http (80) and (8443) internal ports. The internal network is
# 192.168.0.0/24 and the external, public IP address is 10.0.0.1. The
# external interface is eth0.
#
# @param target_addr IP address to forward to
# @param lan_addr internal address of the router
# @param lan_cidr internal IP address range
# @param target_port port number of the forward target
# @param wan_port port number on the external interface
# @param wan_if external interface, public network
# @param wan_cidr publicly available IP address range
# @param source_cidr restrict access to the given IP range
#
# @todo submit upstream https://github.com/voxpupuli/puppet-nftables/issues/236
define profile::nftables::rules::forward(
  Stdlib::IP::Address::V4::Nosubnet $target_addr,
  Stdlib::IP::Address::V4::Nosubnet $lan_addr,
  Stdlib::IP::Address::V4::CIDR $lan_cidr,
  Stdlib::Port $target_port,
  Stdlib::Port $wan_port,
  String[1] $wan_if,
  Stdlib::IP::Address::V4::Nosubnet $wan_addr,
  Optional[Stdlib::IP::Address::V4::CIDR] $source_cidr = undef,
) {
  if $source_cidr {
    $_source_frag = "ip saddr ${source_cidr}"
  } else {
    $_source_frag = ''
  }
  nftables::rule {
    "PREROUTING-dnat_${title}":
      table   => "ip-${nftables::nat_table_name}",
      order   => '41',
      content => "iifname ${wan_if} ${_source_frag} tcp dport ${wan_port} dnat to ${target_addr}:${target_port}";
    # reflection
    "PREROUTING-dnat_${title}reflect":
      table   => "ip-${nftables::nat_table_name}",
      order   => '40',
      content => "tcp dport ${wan_port} ip saddr ${lan_cidr} ip daddr ${wan_addr}/32 dnat to ${target_addr}:${target_port}";
    "POSTROUTING-dnat_${title}reflect":
      table   => "ip-${nftables::nat_table_name}",
      order   => '40',
      content => "tcp dport ${target_port} ip saddr ${lan_cidr} ip daddr ${target_addr}/32 snat to ${wan_addr}";
  }
}
