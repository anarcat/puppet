# a "chat client"
#
# this could be a Matrix, or Jabber, or whatever client but for now
# this is just a tiny shim to login to my irc bouncer role
class profile::chat_client(
  Enum['present','absent'] $ensure = 'present',
  String[1] $ssh_client_dir = '/home/anarcat/.ssh',
) {
  $base_path = "/var/lib/puppet/ssh_keys/${::fqdn}"
  [$key_private, $key_public]  = ssh_keygen("${base_path}/id_ed25519_profile::chat_client", 'ed25519')
  file { "${ssh_client_dir}/id_ed25519_chat":
    mode      => '0400',
    owner     => 'anarcat',
    content   => $key_private,
    show_diff => false,
  }
  file { "${ssh_client_dir}/id_ed25519_chat.pub":
    mode    => '0400',
    owner   => 'anarcat',
    content => $key_public,
  }
  sshd::authorized_key_add { "profile::chat_client-${::fqdn}":
    collect_tag => 'profile::chat_client',
    command     => '/usr/local/bin/mosh-ssh-wrapper dtach -a /run/anarcat-irc/dtach-irssi.socket',
    options     => ['restrict', 'pty'],
    from        => [],
    key         => $key_public,
  }
}
