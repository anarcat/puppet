# an icecast radio
#
# TODO:
#
# - hook into DNS (so that this service migrates or is HA)
# - consider autoradio for HA, if we go there: https://git.autistici.org/ale/autoradio
# - automate let's encrypt setup (right now needs to be done by hand)
class profile::icecast2(
  Boolean $manage_nginx = false,
) {
  # this may better live in Hiera, but the files can't so we have to
  # put those somewhere...
  class { 'icecast2':
    extra_config    => @(EOF),
        <mount>
             <mount-name>/radio.mp3</mount-name>
             <!-- <fallback-mount>/random.ogg</fallback-mount> -->
             <fallback-mount>/fallback-jingle.mp3</fallback-mount>
             <fallback-override>1</fallback-override>
             <!-- i put those here to try to display something on the stats page when there's no client, but it's not working -->
             <stream-name>Currently listening to...</stream-name>
             <stream-description>My playlist of the day. If I'm not listening to anything, you'll hear a silly jingle.</stream-description>
        </mount>
    | EOF
  }
  file { '/usr/share/icecast2/web/fallback-jingle.mp3':
    source => 'puppet:///modules/profile/icecast2/fallback-jingle.mp3',
  }
  file { '/usr/share/icecast2/web/fallback-jingle.ogg':
    source => 'puppet:///modules/profile/icecast2/fallback-jingle.ogg',
  }
  if $manage_nginx {
    include profile::nginx
    class { 'icecast2::nginx':
      # TODO: hiera?
      raw_append => @(EOF),
      # https://www.xmodulo.com/block-specific-user-agents-nginx-web-server.html
      # for more ideas on this
      #
      # Synapse/1.50.0rc1 (b=matrix-org-hotfixes,8e8a00829)
      # Synapse (bot; +https://github.com/matrix-org/synapse)
      if ($http_user_agent ~ Synapse){
        return 403;
      }
      | EOF
    }
  }
}
