# configure a local resolver
#
# TODO: use an existing module? possibilities:
# https://forge.puppet.com/puppet/unbound
class profile::unbound(
  Boolean $manage_networkmanager = true,
) {
  package { 'unbound':
    ensure => 'present',
  }
  service { 'unbound':
    ensure  => 'running',
    require => Package['unbound'],
  }
  file { '/etc/unbound/unbound.conf.d/optimization.conf':
    content => @(EOF),
server:
    # refresh entries in cache automatically
    prefetch: yes
    # prefetch DNSSEC entries to lower latency
    prefetch-key: yes
    # keep expired records around in case network goes down
    serve-expired: yes
    # allow reverse lookups on private adresses
    unblock-lan-zones: yes
    # make +trace work
    access-control: 127.0.0.0/8 allow_snoop
| EOF
    notify  => Service['unbound'],
  }
  # TODO: clear out unbound.conf.d?
  # TODO: manage unbound.conf?

  if $manage_networkmanager {
    file { '/etc/NetworkManager/dispatcher.d/unbound-flush-cache':
      mode    => '0555',
      content => @(EOF),
#!/bin/sh

IFACE=$1
STATE=$2

logger -p daemon.notice -t $0 "starting unbound hook with args $*"

case "$2" in
  up|down)
    logger -p daemon.notice -t $0 "flushing caches..."
    unbound-control reload
    ;;
esac
| EOF
    }
  }
}
