# a postfix "satellite" server which delivers to the central server
# over SSH
class profile::postfix::satellite(
  Optional[String[1]] $relayhost = 'smtp.anarc.at:587',
  Optional[String[1]] $root_mail_recipient = "root@${facts['networking']['domain']}",
) {
  include profile::augeas
  class { 'postfix':
    inet_interfaces     => 'loopback-only',
    satellite           => true,
    manage_mailx        => false,
    relayhost           => $relayhost,
    chroot              => true,
    root_mail_recipient => $root_mail_recipient,
    master_entries      => [
      'smtptlsc  unix  -       -       y       -       -       smtp',
      '    # load the whole file before chroot, we could also hardcode the right cert here',
      '    -o smtp_tls_CAfile=/etc/ssl/certs/ca-certificates.crt',
      '    -o smtp_tls_cert_file=/etc/postfix/x509/client.crt',
      '    -o smtp_tls_key_file=/etc/postfix/x509/client.key',
      '    -o smtp_tls_fingerprint_digest=sha256',
      '    -o smtp_tls_security_level=secure',
      # TODO: this should be upstreamed
      'postlog   unix-dgram n  -       n       -       1       postlogd',
    ],
  }
  postfix::config {
    'default_transport':      value => 'smtptlsc:';
    'maximal_queue_lifetime': value => '30d';
    'message_size_limit':     value => '50240000';
  }

  # remaining diffs:
  # pickup and qmgr are fifos in puppet, unix in debian
  #-pickup    unix  n       -       y       60      1       pickup
  #+pickup    fifo  n       -       y       60      1       pickup
  #-qmgr      unix  n       -       n       300     1       qmgr
  #+qmgr      fifo  n       -       n       300     1       qmgr
  #+# When relaying mail as backup MX, disable fallback_relay to avoid MX loops
  # relay     unix  -       -       y       -       -       smtp
  #-        -o syslog_name=postfix/$service_name
  #+	-o fallback_relay=

  # in main.cf:
  # +virtual_alias_maps = hash:/etc/postfix/virtual
  # not sure that is necessary, especially since the content is the weird:
  # @emma.anarc.at root

  # TODO remaining work:
  #
  # 1. generate TLS certs
  # 2. propagate certs to marcos
}
