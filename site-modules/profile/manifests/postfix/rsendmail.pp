# a postfix "satellite" server which delivers to the central server
# over SSH
class profile::postfix::rsendmail(
  Optional[String[1]] $relayhost = "${facts['networking']['hostname']}-mail@smtp.anarc.at",
  Optional[String[1]] $root_mail_recipient = "root@${facts['networking']['domain']}",
) {
  include profile::augeas
  class { 'postfix':
    inet_interfaces     => 'loopback-only',
    satellite           => true,
    manage_mailx        => false,
    relayhost           => $relayhost,
    chroot              => true,
    root_mail_recipient => $root_mail_recipient,
    # TODO: this should be upstreamed
    master_entries      => [
      'postlog   unix-dgram n  -       n       -       1       postlogd',
      # TODO: consider chrooting?
      'rsendmail unix  -       n       n       -       -       pipe',
      # lint:ignore:single_quote_string_with_variables
      '     user=mail argv=/usr/bin/sshsendmail --host ${nexthop} -f ${sender} ${recipient}',
      # lint:endignore
    ],
  }
  postfix::config {
    'default_transport':      value => 'rsendmail:';
    'maximal_queue_lifetime': value => '30d';
    'message_size_limit':     value => '50240000';
  }

  # remaining diffs:
  # pickup and qmgr are fifos in puppet, unix in debian
  #-pickup    unix  n       -       y       60      1       pickup
  #+pickup    fifo  n       -       y       60      1       pickup
  #-qmgr      unix  n       -       n       300     1       qmgr
  #+qmgr      fifo  n       -       n       300     1       qmgr
  #+# When relaying mail as backup MX, disable fallback_relay to avoid MX loops
  # relay     unix  -       -       y       -       -       smtp
  #-        -o syslog_name=postfix/$service_name
  #+	-o fallback_relay=

  # in main.cf:
  # +virtual_alias_maps = hash:/etc/postfix/virtual
  # not sure that is necessary, especially since the content is the weird:
  # @emma.anarc.at root

  # TODO remaining work:
  #
  # 1. generate SSH key for mail user:
  #
  #        sudo -u mail ssh-keygen -t ed25519 -N '' -f ~mail/.ssh/id_ed25519
  #
  # 2. create a role user for that server (optional)
  #
  #        sudo adduser --system --home /var/lib/$HOSTNAME-mail $HOSTNAME-mail
  #
  # 3. export key in the role user's authorized_keys with
  #    `command="rsendmail",restrict` prefix:
  #
  #        echo 'command="rsendmail",restrict ssh-ed25519 AAAA[...] mail@$HOSTNAME' \
  #            | sudo -u $HOSTNAME-mail tee ~$HOSTNAME-mail/.ssh/authorized_keys
  #
  # 4. install sshsendmail (`pip3 install rsendmail` for now) on the server:
  #
  #    not yet in Debian (#927898, in NEW)
  package { 'rsendmail':
    ensure => 'installed',
  }
  # 5. test delivery
  #
  #        printf "To:anarcat@anarc.at\nSubject: test\n\nThis is a test" | \
  #            sudo -u mail sshsendmail \
  #                         --host $HOSTNAME-mail@smtp.example.com \
  #                         -f root@$HOSTNAME.example.com \
  #                         user@example.com
}
