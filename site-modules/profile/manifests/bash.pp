# configure some basic bash things
class profile::bash {
  package { [
    'bash',
    'bash-completion',
  ]:
    ensure => present,
  }
  # send a bell on command completion
  #
  # we don't do this for all users, because it requires a special
  # configuration in the terminal to not be absolutely and completely
  # annoying. Also this overrides the default bashrc shipped from
  # /etc/skel/.bashrc, but that one is basically empty.
  file { '/root/.bashrc':
    content => @(EOF),
    # send a bell when command completes
    PROMPT_COMMAND='printf "\a"'
    | EOF
  }
}
