# developer tools: VCS, editor, emulation...
class profile::devel {
  include profile::emacs
  include profile::libvirt
  include profile::podman
  include sbuild::unshare
  package { [
    '2to3', # first rough pass tool to port to Python 3
    'adb',
    'adequate', # debian package testing
    'alien', # used to make .deb out of RPM pkgs for crap from Dell
    'apt-listbugs',
    'aptitude',
    'austin', # python profiler
    'bats', # bash unit testing
    'binwalk',
    'black', # Python code formatting
    'build-essential',
    'cdbs',
    'clangd', # C LSP server
    'cloc',
    'codespell', # spell checker for code
    'colordiff',
    'cvs',
    'dateutils', # operations like add, diff, etc
    'debmake', # to build basic C packages
    'decopy', # copyright inspection
    'dgit',
    'dh-make',
    'dh-make-golang',
    'devscripts',
    #'dia', # in graphics
    'dput-ng',
    'exuberant-ctags',
    'fabric',
    'fastboot',
    'flake8',
    'gdb',
    'git-buildpackage',
    'git-email',
    'git-extras',
    'git-mediawiki',
    'git-review',
    # 'git-sizer', # to find large objects in a repo and other cool stuff
    'git-svn',
    'gitlab-cli', # used by tpo/tpa/gitlab-tools
    'gitlint',
    'glade',
    'gnome-boxes', # still no working Ubuntu 12.04 on curie, TODO: replace with virt-manager when we remove ubuntu 12.04 aka gnt-chi
    'golang',
    'graphviz',
    'hyperfine', # micro-benchmarks for shell commands
    'hugo', # static site generator used at work for some sites (e.g. status.tpo)
    'isort', # sort python import statements consistently
    'help2man',
    'how-can-i-help',
    'hub',
    'icdiff',
    'ikiwiki',
    'info',
    'inotify-tools',
    'ipython3',
    'ldap-utils',
    'librarian-puppet',
    'libterm-readkey-perl',
    'libsearch-xapian-perl',
    'libdevel-nytprof-perl', # for flamegraph
    'linkchecker',
    'make-doc',
    'mercurial',
    'mypy', # python type checking
    'myrepos',
    'npm',
    'num-utils', # sum/avg/etc on the cli
    'pastebinit',
    'perl-doc',
    'po4a',
    'puppet-lint',
    'puppet-strings',
    'pxelinux', 'syslinux-efi', # for bootloaders in VMs
    'pypi2deb',
    'python3',
    'python3-betamax',
    'python3-build',
    'python3-doc',
    'python3-jedi',
    'python3-html2text',
    'python3-pip',
    'python3-pytest',
    'python3-seaborn',
    'python3-setuptools-scm',
    'python3-sphinx',
    'python3-sphinx-rtd-theme',
    'python3-typeshed',
    'python3-unidecode',
    'python3-vcr',
    'python3-venv',
    'quilt',
    'reprotest', # test debian packages for reproducibility
    'g10k',
    'r10k', # for linting
    'ruby-rspec',
    'shellcheck',
    'sloccount',
    'sqlitebrowser',
    'subversion',
    'tox',
    'twine',
    'ubuntu-dev-tools',
    'vagrant',
    'valgrind',
    'virt-manager',
    'yamllint',
  ]:
    ensure => present,
  }
  package { [
    'apt-venv', # removed from bullseye, unused #979347
    'bzr', # replaced by brz in bookworm, but unused anyways
    'debian-installer-10-netboot-amd64', # unused
    'elpa-py-autopep8', # replaced with pylsp and friends
    'github-backup', # removed from Debian bookworm
    'gocode', # removed from Debian, replace with LSP
    'go-dep', # abandoned upstream #978163
    'ikiwiki-hosting-common', # only used on the server, not needed in dev
    'kicad', # never used
    'libtext-bibtex-perl', # only on the server?
    'multitime', # replaced by hyperfine
    'python-ttystatus', # never used
    'stylish-haskell', # unused
    'virtualbox', # not in debian
  ]:
    ensure => 'purged',
  }
  # python2 packages
  package { [
    'ipython',
    'python',
    'python-wheel',
    'python-sphinx-rtd-theme',
    'python-sphinx',
    'python-setuptools',
    'python-jedi',
    'python-pip',
    'python-pytest',
    'python-seaborn',
  ]:
    ensure => 'purged',
  }
}
