# configure puppet agents
class profile::puppet {
  include profile::augeas
  class {'::puppet':
    runmode       => 'systemd.timer',
    # default puppetmaster is $fqdn, which is silly: we want to
    # connect to a puppetmaster by default!
    # https://github.com/theforeman/puppet-foreman/issues/864
    puppetmaster  => 'puppet.anarc.at',
    auth_template => 'profile/auth.conf.erb',
    # TODO: doesn't this belong in role::server:puppet?
    dns_alt_names => [
      'puppet',
      'puppet.anarc.at',
      'puppet.orangeseeds.org',
      'marcos.anarc.at',
      'marcos.orangeseeds.org',
    ],
  }
  file { '/etc/puppet/fileserver.conf':
    content => @(EOF),
    # private file server
    [files]
        path /etc/puppet/files
    | EOF
  }
  # handy shortcuts
  file { '/usr/local/bin/pat':
    mode    => '0555',
    content => @(EOF),
    #!/bin/sh
    puppet agent --onetime --verbose --no-daemonize --show_diff --no-usecacheonfailure "$@" "$@"
    ret="$?"
    printf '\a'
    exit $ret
    |EOF
  }
  file { '/usr/local/bin/patn':
    mode    => '0555',
    content => @(EOF),
    #!/bin/sh
    puppet agent --onetime --verbose --no-daemonize --show_diff --no-usecacheonfailure --noop "$@"
    ret="$?"
    printf '\a'
    exit $ret
    |EOF
  }
}
