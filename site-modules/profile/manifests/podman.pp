# a docker replacement
class profile::podman(
  Enum['present','absent'] $ensure = 'present',
) {
  package { 'podman':
    ensure => $ensure,
  }
  package { 'podman-docker':
    ensure => $ensure,
  }
  # podman-docker conflicts with docker
  class { 'docker':
    ensure                      => 'absent',
    use_upstream_package_source => false,
    docker_ce_package_name      => 'docker.io',
    docker_ce_start_command     => '',
    # the module template puts dockerd in /usr/bin/ instead of
    # /usr/sbin/, which is where the docker.io Debian package (more
    # logically, IMHO) put it. just skip the override altogether to
    # bypass that.
    service_overrides_template  => false,
  }

  # TODO:
  # anarcat@curie:~$ cat /etc/containers/storage.conf
  # [storage]
  # driver = "zfs"
}
