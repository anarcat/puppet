# special configurations for etckeeper, for servers that do use it
class profile::etckeeper {
  package { 'etckeeper':
    ensure => installed,
  }
  file { '/etc/.gitignore':
    source => 'puppet:///modules/profile/etckeeper/gitignore',
  }

  # override builtin metadata script to fix ignores and optimize the
  # common use case. those patches were sent upstream as:
  # https://etckeeper.branchable.com/todo/optimize_mode_metadata:_ignore_defaults/
  # https://etckeeper.branchable.com/todo/metadata_ignore_filters_do_not_work/
  file { '/etc/etckeeper/pre-commit.d/30store-metadata':
    source => 'puppet:///modules/profile/etckeeper/30store-metadata',
  }
}
