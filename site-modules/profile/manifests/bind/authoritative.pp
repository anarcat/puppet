# an authoritative nameserver implemented with bind
#
# TODO: this is just a minimal replica of the marcos config. we should
# also configure the zonefiles and all that jazz, of course... and
# even better, connect services to the bind server so that they are
# automatically added to zone files (and yes, serials incremented too,
# why not)
#
# TODO: DNSSEC. last time we tried this, we had to flip the zones to
# `dynamic => true`, and that meant updates to the zone files weren't
# written on the hosts, so we might need to first do the above
# (ie. convert to records)
#
# here's an example of how this could be done:
#
# $records = lookup('profile::bind::records', Hash, 'first', {})
#
# $records.each |$record, $config| {
#    $params = {
#      # if empty, use resource title
#      'record' => $config['record'] ? {
#        undef   => $record,
#        default => $config['record']
#      },
#      # A or AAAA if IP address detected, otherwise default to CNAME
#      'type'   => $config['type'] ? {
#        undef   => if is_ipv4_address($config['data']) { 'A' } else { 'CNAME' },
#        default => $config['type']
#      },
#      'server' => '127.0.0.1',
#      'ttl'    => 43200,
#      'hmac'   => 'hmac-sha256',
#      'secret' => trocla('update-key@bind9', 'plain')
#    } + $config.delete('ptr')
#
#    ensure_resource('resource_record', $record, $params)
#
#    if $params['type'] == 'A'
#      and !(member(keys($zones), $params['record']))
#      and $config['ptr'] {
#
#      # build reverse zone
#      $rzone_prefix =  join(delete_at(reverse(split($params['data'], '\.')), 0), '.')
#      $rzone = "${rzone_prefix}.in-addr.arpa"
#
#      # build reverse PTR record
#      $rrecord = "${record}-ptr"
#      $digit = split($params['data'], '\.')[3]
#      $rparams = {
#        ensure => $params['ensure'],
#        type   => 'PTR',
#        record => "${digit}.${rzone}",
#        data   => "${params['record']}.",
#        zone   => $rzone,
#        server => '127.0.0.1',
#        ttl    => $params['ttl'],
#        hmac   => $params['hmac'],
#        secret => $params['secret'],
#      }
#
#      ensure_resource('resource_record', $rrecord, $rparams)
#    }
#
# profile::bind::records:
#  example.com:
#    type: 'A'
#    data: ['999.999.999.999', '999.999.999.999', '999.999.999.999', '999.999.999.999']
#    ttl: 3600
#  example.com-ns:
#    type: 'NS'
#    ttl: 1800
#    record: 'example.com'
#    data: ['ns1.zonerisq.ca.', 'ns2.zonerisq.ca.' ]
#  example.com-mx:
#    type: 'MX'
#    ttl: 3600
#    record: 'example.com'
#    data: '0 example-com.mail.eo.outlook.com.'
#  example.com-txt:
#    type: 'TXT'
#    ttl: 3600
#    record: 'example.com'
#    data:
#      - 'v=spf1 include:mail.example.com include:spf.foo.ca include:spf.protection.outlook.com mx ~all'
#  example.com-srv-sipfed:
#    type: 'SRV'
#    record: '_sipfederationtls._tcp.example.com'
#    data: '100 1 5061 sipfed.online.lync.com.'
#  ns1.example.com: { data: '999.999.999.999', ptr: true }
#  ns2.example.com: { data: '999.999.999.999', ptr: true }
#  name.example.com: { data: '999.999.999.999', ptr: true }
#  foo.example.com { data: '999.999.999.999', ptr: true }
#  bar.example.com { data: 'foo.example.com.', ptr: true }
#  '*.wildcard.example.com': { data: 'foo.example.com.' }
class profile::bind::authoritative (
  Boolean $dnssec = true,
  Hash $keys = {},
  Hash $acls = {},
  Hash $zones = {},
  Hash $views = {},
) {
  class { 'profile::bind':
    dnssec => $dnssec,
  }
  bind::view { 'default':
    recursion => false,
    zones     => ['orangeseeds.org', 'anarc.at'],
  }
  bind::zone { 'orangeseeds.org':
    zone_type => 'master',
    dynamic   => false,
    source    => 'puppet:///files/zones/orangeseeds.org.db', # lint:ignore:puppet_url_without_modules
  }
  bind::zone { 'anarc.at':
    zone_type => 'master',
    dynamic   => false,
    source    => 'puppet:///files/zones/orangeseeds.org.db', # lint:ignore:puppet_url_without_modules
  }
}
