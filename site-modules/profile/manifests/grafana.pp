# install grafana in a docker container
class profile::grafana {
  include profile::docker
  file { '/etc/docker/grafana':
    ensure => directory,
  }
  -> file { '/etc/docker/grafana/docker-compose.yml':
    ensure => present,
    source => 'puppet:///modules/profile/compose-grafana.yml',
  }
  # this will start the container(s)
  -> docker_compose { 'grafana':
    ensure        => present,
    compose_files => ['/etc/docker/grafana/docker-compose.yml'],
  }
  -> cron { 'update-grafana':
      command => 'docker-compose -f /etc/docker/grafana/docker-compose.yml -p grafana pull && docker-compose -f /etc/docker/grafana/docker-compose.yml -p grafana up -d', # lint:ignore:140chars
      user    => 'root',
      hour    => 5,
      minute  => 35,
      weekday => 1,  # monday
    }
}
