# a webmail, any webmail?
class profile::webmail::alps(
  Enum['present','absent'] $ensure = 'present',
) {
  include profile::docker
  file { '/etc/docker/alps/':
    ensure => directory,
  }
  -> file { '/etc/docker/alps/docker-compose.yml':
    ensure => $ensure,
    source => 'puppet:///modules/profile/compose-alps.yml',
  }
  # this will start the container(s)
  -> docker_compose { 'alps':
    ensure        => $ensure,
    compose_files => ['/etc/docker/alps/docker-compose.yml'],
    require       => File['/etc/docker/alps/docker-compose.yml'],
  }
}
