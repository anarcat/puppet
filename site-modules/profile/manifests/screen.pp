# configure the venerable screen(1) command
#
# This is just too useful to live without, especially to switch
# between physical console and remote SSH during debugging sessions
#
# normally, I should probably just bite the bullet and switch to tmux
# already, but my muscle memory is just too bound to screen(1) right
# now that I wouldn't be able to manage the transition with at least a
# backwards compatible key mapping, but also a `screen` shell alias as
# well.
#
# anyways, point is, i still use screen, so configure it the right way
# here
class profile::screen {
  package { 'screen':
    ensure => present,
  }
  file { '/etc/screenrc':
    source => 'puppet:///modules/profile/screenrc',
  }
}
