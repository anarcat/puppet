# a seedbox is basically a bittorrent client, but could be more?
class profile::seedbox(
  String[1] $user = 'admin',
  # TODO: generate salted passwords, see
  # https://github.com/transmission/transmission/blob/611d36ac84f24adf4b4237b49a411cba3276ca60/tests/libtransmission/session-test.cc#L154
  String[1] $password = trocla("profile::seedbox::${::hostname}::${user}", 'plain'),
  Integer $speed_limit_up = undef,
  Integer $speed_limit_down = undef,
  Integer $alt_speed_limit_up = 300,
  Integer $alt_speed_limit_down = 1000,
  String[1] $download_root = '/var/lib/transmission-daemon',
) {
  package { [
    'git-annex', # to record downloads
    'yt-dlp', # another kind of downloader
  ]:
  }
  class { 'transmission':
    manage_ppa                   => false,
    alt_speed_down               => $alt_speed_limit_down,
    alt_speed_up                 => $alt_speed_limit_up,
    alt_speed_time_enabled       => true,
    alt_speed_time_day           => 62,
    alt_speed_time_begin         => 540,
    alt_speed_time_end           => 1140,
    download_queue_size          => 2,
    # download_root is, e.g. /var/lib/transmission-daemon/ then
    # downloads go in downloads/, there's also watch etc.
    download_root                => $download_root,
    download_dir                 => 'downloads',
    encryption                   => 2,
    incomplete_dir               => 'downloads',
    lpd_enabled                  => true,
    # for some reason, the upstream module thinks that should have
    # been /home/transmission-daemon: it's really in /var.
    home_dir                     => '/var/lib/transmission-daemon',
    message_level                => 1,
    peer_limit_global            => 100,
    peer_limit_per_torrent       => 20,
    peer_port                    => 10000, # TODO: punch hole in firewall
    port_forwarding_enabled      => false, # we don't use UPNP, but maybe we should?
    rename_partial_files         => false, # don't suffix `.part` for partial downloads
    rpc_authentication_required  => true,
    rpc_username                 => $user,
    rpc_password                 => $password,
    rpc_bind_address             => '127.0.0.1', # allow only from localhost
    rpc_whitelist                => '127.0.0.1', # allow only from localhost
    speed_limit_up               => $speed_limit_up, # KiB/s
    speed_limit_down             => $speed_limit_down,
    speed_limit_up_enabled       => true,
    speed_limit_down_enabled     => true,
    script_torrent_done_enabled  => true,
    # TODO: deploy from puppet
    script_torrent_done_filename => '/usr/local/bin/transmission-git-annex-add',

    # drop from marcos config:
    # "download-limit": 100,
    # "download-limit-enabled": 0,
    # "max-peers-global": 200, # legacy, now peer-limit-global
    # "peer-limit-global": 100,
    # "peer-limit-per-torrent": 20,
    # "upload-limit": 100,
    # "upload-limit-enabled": 0,
    # "seed-queue-size": 8,
    # "watch-dir": "/home/media/auto",
    # "watch-dir-enabled": true
    # added by module:
    # "lazy-bitfield-enabled": true,
    # "seed-queue-size": 10,
  }

  systemd::dropin_file { 'limits.conf':
    unit    => 'transmission-daemon.service',
    content => @(EOF),
    # File managed by Puppet, local changes will be lost.
    [Service]
    IOSchedulingClass = best-effort
    IOSchedulingPriority = 7
    Nice = 10
    | EOF
  }
}
