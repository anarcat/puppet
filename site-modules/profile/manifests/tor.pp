# our base Tor configuration
class profile::tor(
  Hash[String,Hash] $onion_services = {
    'onion-ssh' => {
      ports => ['22'],
      single_hop => true,
    }
  },
) {
  include tor

  create_resources('tor::daemon::onion_service', $onion_services)
}
