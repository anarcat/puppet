# my emacs packages
class profile::emacs(
  Boolean $gui = true,
) {
  $emacs_package = $gui ? {
    true => 'emacs',
    false => 'emacs-nox',
  }
  package { $emacs_package:
    ensure => present,
  }

  include profile::lsp
  package { [
    'auctex', # emacs' latex mode
    'dh-make-elpa',
    'gettext-el',
    'elpa-anzu',
    'elpa-atomic-chrome',
    'elpa-auto-dictionary',
    'elpa-consult', # menu overrides, grep, etc
    'elpa-corfu',
    'elpa-debian-el',
    'elpa-dpkg-dev-el',
    'elpa-dimmer',
    'elpa-dockerfile-mode',
    'elpa-esup', # .emacs profiler
    'elpa-format-all', # do-it-all formatter
    'elpa-flycheck',
    'elpa-git-timemachine',
    'elpa-gitlab-ci-mode',
    'elpa-go-mode',
    'elpa-hl-todo',
    'elpa-imenu-list',
    'elpa-ledger',
    'elpa-lsp-mode',
    'elpa-lsp-ui',
    'elpa-magit',
    'elpa-magit-todos',
    'elpa-mailscripts',
    'elpa-markdown-mode', # buggy, #1036359, see md_toc too
    'elpa-markdown-toc',
    'elpa-marginalia',
    'elpa-message-templ', # message templates, for emails in emacs
    'elpa-minimap', # see the entire buffer at once
    'elpa-notmuch',
    'elpa-php-mode',
    'elpa-pip-requirements',
    'elpa-projectile',
    'elpa-puppet-mode',
    'elpa-rainbow-mode',
    'elpa-rg', # ripgrep frontend
    'elpa-smart-mode-line',
    'elpa-use-package',
    'elpa-vertico',
    'elpa-wc-mode',
    'elpa-web-mode',
    'elpa-which-key',
    'elpa-writegood-mode',
    'elpa-writeroom-mode',
    'elpa-ws-butler',
    'elpa-yaml-mode',
    'elpa-yasnippet',
    'emacs-el',
    'emacs-goodies-el',
    'emacs-common-non-dfsg',
    'org-mode',
    'org-mode-doc',
  ]:
    ensure => present,
  }

  package { [
    'elpa-company-go', # removed from Debian, replaced with LSP #976642
    'elpa-elpy', # unmaintained in Debian, replaced with lsp-mode
    'elpa-haskell-mode', # unused, missing from bookworm
    'elpa-solarized-theme', # replaced with srcery, not in Debian
    'elpa-undo-tree', # leaves ~undo-tree~ crap everywhere, confusing
  ]:
    ensure => 'purged',
  }
}
