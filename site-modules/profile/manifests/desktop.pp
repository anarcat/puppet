# a "desktop" computer, which can also be a laptop: basically a
# workstation of some sort.
#
# @param boolean wayland enable wayland session
class profile::desktop(
  Boolean $wayland = false,
) {
  include profile::gnupg
  include profile::firefox
  include profile::mpd
  # experiment with pipewire as a pulseaudio replacement
  include profile::pipewire
  include profile::tpo
  include profile::unbound
  if $wayland {
    include profile::sway
    class { 'profile::i3':
      ensure => absent,
    }
  } else {
    include profile::i3
  }

  # TODO there is *so* much more shit to do here:
  # - ~/.local/share/fonts/download.sh
  # - the /etc/X11/Xsession.options hack to disable ssh-agent *and* somehow disable gnome-keyring
  # - mime associations, see https://www.enricozini.org/blog/2020/debian/mime-type-associations/
  # - firefox extensions and config, see https://anarc.at/software/desktop/firefox/
  # - ssh keys propagation (irc.anarc.at, smd)
  # - probably way more
  package { [
    'afuse',
    'apksigner', # wtf?
    'aspell-en',
    'aspell-fr',
    'atril', # PDF/ebook viewer, see also https://anarc.at/software/desktop/calibre/
    'baresip', # makes phone calls
    'blueman', # bluetooth applet
    'bsdgames', # for wtf
    'btop', # cool system monitoring tool, actually fired from my status bar
    'calibre', # manage ebooks: calibredb add or calibredb add -1 <folder>
    'chromium',
    'convmv', # to rename files between encoding
    'cups-browsed', # to find printers? not sure
    'ddccontrol', # for changing monitor lighting, while the kernel learns about this
    'ddrescueview', # archival tool
    'default-jdk-headless',
    'diceware',
    'figlet', # generate ascii art out of text, see also toilet, sysvbanner
    'fonts-firacode',
    'fonts-roboto',
    'fortunes', # wtf?
    'fwupd-amd64-signed', # firmware upgrades
    'git-annex',
    'git-lfs',
    'gnome-power-manager', # to display battery stats
    'gnutls-bin', # wtf
    'gobby',
    'grammalecte-cli', # for flycheck-grammalecte, see also libreoffice-grammalecte
    'gstreamer1.0-tools', # for screentest
    'gucharmap',
    'guvcview', # webcam control, used to tilt the viewer, from https://askubuntu.com/questions/85763/how-can-i-control-the-pan-tilt-lighting-and-other-features-on-a-logitech-quic
    'hledger',
    'iamerican-huge', 'ifrench', 'wamerican-huge', 'wfrench', # for aspell/ispell
    'intel-gpu-tools', # intel_gpu_top
    'jmtpfs',
    'khal',
    'khard',
    'kstars',
    'ledger',
    'less',
    'libinput-tools', # for libinput list-devices and debug-tablet
    'libnotify-bin',
    'libreoffice',
    'libreoffice-gtk3', # nicer GUI than the default, which doesn't really work
    'libreoffice-grammalecte',
    'libreoffice-l10n-fr', 'hunspell-fr-classical', 'mythes-fr',
    'libxkbcommon-tools', # show keysym names with: xkbcli interactive-wayland or interactive-x11
    'locales',
    'magic-wormhole', # to transfer files easily
    'man-db', # documentation. it's good.
    'manpages',
    'mesa-utils', # glxgears, glxinfo
    'mumble',
    'ncal',
    'needrestart-session',
    'network-manager-gnome',
    'network-manager-openvpn-gnome',
    'oathtool',
    'onionshare',
    'opensnitch',
    'pass',
    'pass-otp',
    'pavucontrol',
    'picard',
    'pinpoint',
    'pmount',
    'psensor',
    'pubpaste',
    'python3-qrcode', # handy qr(1) tool
    'qalc',
    'qalculate-gtk',
    'qreator', # GUI to create qrcodes
    'redshift-gtk',
    'reportbug', # to... euh... report bugs
    'riseup-vpn', # excellent VPN ran by trusted friends
    'safeeyes', # workrave replacement
    'signing-party', # monkeysign replacement
    'sioyek', # PDF and EPUB viewer, see also calibre above
    'sm',
    'speedtest-cli',
    'sq', # Rust and sane OpenPGP implementation
    'sxiv',
    'syncthingtray', # monitor syncthing, distributed file transfers
    'tellico',
    'thunar', # file manager, alternative: pcmanfm
    'torbrowser-launcher',
    'transmission-cli', # for batch operations on transmission
    'transmission-qt',
    'tremotesf', # alternative Transmission client, supports HTTPS endpoints
    'udftools',
    'undertime',
    'unicode',
    'urlscan',
    'usbguard-notifier', # to make usbguard more functional, if installed
    'usbutils', # for lsusb
    'vdirsyncer',
    'verbiste',
    'verbiste-gtk',
    'whipper', # to rip CDs
    'wifi-qr', # exchange wifi APs over qr codes
    'wireless-tools',
    'x11-apps', # xclock, xeyes, useful even in Wayland
    'xkcdpass',
    'xournalpp',
    'xplanet',
    'zathura', # PDF viewer, see also calibre above
    'zbar-tools',
  ]:
    ensure => present,
  }
  package { [
    'anki', # not doing spaced reps on my laptop
    'electrum', # not used anymore, screw cryptobros and their world. SELL.
    'exiftool', # not used anymore
    'finger', # dead protocol
    'gajim', # not using jabber anymore
    'gameclock', # dead: not ported to GTK3/Python3 :(
    'git-annex-remote-rclone', # not used anymore
    'libghc-xmonad-dev', # xmonad dev libs, required for run, unused
    'libghc-xmonad-contrib-dev',
    'libghc-xmonad-extras-dev',
    'libghc-taffybar-dev',
    'monkeysign', # dead: not ported to GTK3/Python3 :(
    'monkeysphere', # dead: abandoned, FTBFS #1011900
    'network-manager-iodine-gnome', # removed from debian, #908017
    'nheko', # replaced with fluffychat
    'parcimonie', # not used anymore?
    'pasystray', # replaced with waybar's wireplumber plugin, see also https://wiki.archlinux.org/title/PulseAudio#Graphical
    'pidgin', # not using jabber, or ICQ for that matter, anymore
    'python-certifi', # python3, unused now?
    'ranger', # not used
    'surfraw', # not used
    'syncthing-gtk', # replaced by syncthingtray
    'taffybar', # missing from bookworm, unused
    'trayer', # used in xmonad, not used anymore
    'tty-clock', # RC bug, not really maintained
    'usbguard-applet-qt', # removed from upstream, see
                          # https://tracker.debian.org/news/1069337/accepted-usbguard-075ds-1-source-into-unstable/
    'webext-ublock-origin', # transitional package
    'workrave', # replaced by safeeyes, prettier, better maintained
    'xmobar', # missing from bookworm, unused
    'xmonad', # not used anymore
    'xournal', # replaced by xournalpp
    'zotero-standalone', # deployed through flatpak
  ]:
    ensure => purged,
  }

  class { 'flatpak':
    manage_repo => false,
  }
  flatpak_remote { 'flathub':
    ensure   => present,
    location => 'https://flathub.org/repo/flathub.flatpakrepo',
  }
  flatpak { [
    # not packaged in Debian
    'org.signal.Signal',
    # gone from Debian
    'org.zotero.Zotero',
    # not in Debian (yet) https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=986232
    'app.organicmaps.desktop',
    # Matrix client, not (yet?) in Debian
    'im.fluffychat.Fluffychat',
  ]:
    ensure => present,
    remote => 'flathub',
  }
  # we used the flatpak version for a while because the Debian package
  # was out of date, which was causing sync errors with ebook readers
  # (e.g. recent Kobo). now that is not an issue anymore because I
  # sync those files with syncthing (which, yes, runs on the kobo) and
  # Calibre is recent enough in Debian (bookworm) anyway
  #
  # the other reason was that we do not trust the calibre source code
  # very much and prefer to run it sandboxed... but that is too
  # inconvenient: crucial utilities like calibredb are not available
  # from the flatpak
  flatpak { 'com.calibre_ebook.calibre':
    ensure => absent,
  }
  file { '/var/lib/flatpak/overrides/':
    ensure => directory,
  }

  file { '/usr/local/bin/fluffychat':
    ensure  => 'link',
    target  => '/var/lib/flatpak/app/im.fluffychat.Fluffychat/current/active/export/bin/im.fluffychat.Fluffychat',
    require => Flatpak['im.fluffychat.Fluffychat'],
  }
  file { '/usr/local/bin/organicmaps':
    ensure  => 'link',
    target  => '/var/lib/flatpak/app/app.organicmaps.desktop/current/active/export/bin/app.organicmaps.desktop',
    require => Flatpak['app.organicmaps.desktop'],
  }
  file { '/usr/local/bin/signal':
    ensure => 'absent',
  }
  file { '/etc/chromium.d/disable-compositing':
    ensure => absent,
  }
  if $wayland {
    file { '/etc/chromium.d/wayland':
      content => @(EOF),
      # tell chrome to use wayland
      # crashes on startup, see https://github.com/flathub/org.signal.Signal/issues/515
      export CHROMIUM_FLAGS="$CHROMIUM_FLAGS --enable-features=WaylandWindowDecorations --ozone-platform-hint=auto"
      | EOF
    }
    file { '/usr/local/bin/signal-desktop':
      mode    => '0555',
      content => @(EOF),
      #!/bin/sh
      #
      # enable Wayland support in Electron, taken from:
      #
      # https://wiki.archlinux.org/title/Wayland#Electron
      #
      # this is basically:
      #
      # /var/lib/flatpak/app/org.signal.Signal/current/active/export/bin/org.signal.Signal
      #
      # with an extra flag to enable Wayland.
      exec /usr/bin/flatpak run --branch=stable --arch=x86_64 org.signal.Signal \
          --enable-features=WaylandWindowDecorations --ozone-platform-hint=auto "$@"
      | EOF
      require => Flatpak['org.signal.Signal'],
    }
  } else {
    file { '/usr/local/bin/signal-desktop':
      ensure  => 'link',
      target  => '/var/lib/flatpak/app/org.signal.Signal/current/active/export/bin/org.signal.Signal',
      require => Flatpak['org.signal.Signal'],
    }
  }
  file { '/usr/local/bin/zotero':
    ensure  => 'link',
    target  => '/var/lib/flatpak/app/org.zotero.Zotero/current/active/export/bin/org.zotero.Zotero',
    require => Flatpak['org.zotero.Zotero'],
  }
  # https://github.com/flathub/org.zotero.Zotero/issues/65
  file { '/var/lib/flatpak/overrides/org.zotero.Zotero':
    content => @(EOF),
    [Context]
    filesystems=!home;~/.zotero/
    | EOF
    require => [
      Flatpak['org.zotero.Zotero'],
      File['/var/lib/flatpak/overrides/'],
    ],
  }
  file { '/usr/local/bin/calibre':
    ensure  => 'absent',
  }
  # https://github.com/flathub/com.calibre_ebook.calibre/issues/51
  file { '/var/lib/flatpak/overrides/com.calibre_ebook.calibre':
    ensure => absent,
  }
  file { '/etc/sudoers.d/anarcat-power':
    content => @(EOF),
    anarcat ALL=NOPASSWD: /bin/journalctl -f
    anarcat ALL=NOPASSWD: /usr/bin/journalctl -f
    anarcat ALL=NOPASSWD: /usr/bin/ddccontrol
    anarcat ALL=NOPASSWD: /sbin/halt ""
    anarcat ALL=NOPASSWD: /usr/sbin/halt ""
    anarcat ALL=NOPASSWD: /sbin/reboot ""
    anarcat ALL=NOPASSWD: /usr/sbin/reboot ""
    anarcat ALL=NOPASSWD: /sbin/shutdown -r
    anarcat ALL=NOPASSWD: /usr/sbin/shutdown -r
    anarcat ALL=NOPASSWD: /sbin/shutdown -H
    anarcat ALL=NOPASSWD: /usr/sbin/shutdown -H
    anarcat ALL=NOPASSWD: /bin/systemctl poweroff
    anarcat ALL=NOPASSWD: /usr/bin/systemctl poweroff
    anarcat ALL=NOPASSWD: /bin/systemctl suspend
    anarcat ALL=NOPASSWD: /usr/bin/systemctl suspend
    anarcat ALL=NOPASSWD: /bin/systemctl hibernate
    anarcat ALL=NOPASSWD: /usr/bin/systemctl hibernate
    anarcat ALL=NOPASSWD: /bin/systemctl reboot
    anarcat ALL=NOPASSWD: /usr/bin/systemctl reboot
    | EOF
  }

  # TODO: steam.
  # dpkg --add-architecture i386
  # apt update
  # apt install steam
  # accept license agreement...
  # then there's a whole other step of:
  # 1. running the steam command, which...
  # 2. ... upgrades steam
  # 3. ... runs some "pin" thing
  # 4. then login with steam, which...
  # 5. ... sends a confirmation email because this is a new machine
  # 6. then download the games i want to play from my library
  # ... those steps are unlikely to be automatable
}
