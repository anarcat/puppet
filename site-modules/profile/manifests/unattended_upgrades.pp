# this class overrides some parameters from the unattended-upgrades module
#
# this could be moved to hiera, but it was cargo-culted from
# tor-puppet, so we want to minimize this diff
class profile::unattended_upgrades(
  Boolean $auto_reboot = false,
  Array[String[1]] $origins = [
    # lint:ignore:single_quote_string_with_variables
    'origin=Debian,codename=${distro_codename},label=Debian',
    'origin=Debian,codename=${distro_codename},label=Debian-Security',
    # bullseye and later
    'origin=Debian,codename=${distro_codename}-security,label=Debian-Security',
    # lint:endignore
  ],
) {
  class { 'unattended_upgrades':
    # age ({}): A hash of settings with two possible keys:
    #
    # min (2): Minimum age of a cache package file. File younger than min will not be deleted.
    # max (0): Maximum allowed age of a cache package file. File older than max will be deleted.
    age                    => { 'max' => 10 },
    # A hash of settings with these possible keys:
    # clean(0): Remove packages that can no longer be downloaded
    #   from cache every X days (0 = disabled).
    # fix_interrupted_dpkg(true): Try to fix package installation state.
    # reboot(false): Reboot system after package update installation.
    # reboot_time(now): If automatic reboot is enabled and needed,
    #   reboot at the specific time (instead of
    #   immediately). Expects a string in the format "HH:MM", using
    #   the 24 hour clock with leading zeros. Examples: "16:37" for
    #   37 minutes past 4PM, or "02:03" for 3 minutes past 2AM.
    # remove(true): Remove unneeded dependencies after update installation.
    auto                   => {
      'clean'  => 7,
      'remove' => true,
      'reboot' => $auto_reboot,
    },
    # Split the upgrade process into sections to allow shutdown during upgrade.
    minimal_steps          => false,
    origins                => $origins,
    blacklist              => [
      'grub-pc',
    ],
    # takes effect only after unattended-upgrades 2.5 (bullseye)
    remove_new_unused_deps => true,
    remove_unused_kernel   => true,
  }
}
