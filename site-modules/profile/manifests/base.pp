# base class included everywhere
#
# packages here should have minimal dependencies (e.g. no GUI)
class profile::base(
  String[1] $ntpd = 'chrony',
) {
  include profile::base::network
  include profile::screen
  # TODO: remove sudo package below and replace with doas, still
  # missing some sudoers.d file, probably will require cirrax/doas
  include profile::doas
  package { [
    'apparmor', # Linux Security Module to harden packages
    'apparmor-profiles', # ship default profiles to harden many things
    'apparmor-profiles-extra', # and even the extras
    'apparmor-utils', # aa-enforce, etc
    'apt-file', # to find files matching a package, too often used everywhere
    'apt-listchanges', # yell about NEWS entries and changelogs on upgrades
    'at', # run shell scripts in the future
    'borgbackup', # all servers need backups
    'borgmatic', # TODO: automatic borg backup configuration
    'bsdextrautils', # hd, hexdump, write
    'bsdiff', # binary diff
    'bzip2', # frequently used compressor
    'busybox', # rescue shell for emergency situations
    'dasel', # like jq and yq, but also CSV, XML, TOML...
    'debconf-utils', # for backups with debconf-get-selections
    'deborphan', # used in upgrades
    'debsums', # checksum files of debian packages, debsums -a also checks confs
    'dmsetup', # for battling the device mapper
    'dstat', # important sysadmin diagnostic tool
    'fail2ban', # to block repeated logging attempts, TODO: basic configuration?
    'fd-find', # much faster and intuitive find(1) replacement
    'fdisk', # edit partitions on disk, also includes sfdisk
    'gdisk', # similar but for gpt partitions, alternative: parted
    'git', # used all the time
    'htop', # basic live monitoring
    'installation-birthday', # to answer the question "how old is this installe"
    'iotop-c', # "who eats the I/O"
    'iputils-ping', # venerable ping, without suid
    'jq', # JSON swiss army knife, used all the time, see also yq, dasel
    'localepurge', # purge unused locales to save disk space
    'lshw', # basic hardware inventory (replaces default "discover" from d-i)
    'lsof', # list open files, see also netstat or ss
    'memtest86+', # memory tests
    'molly-guard', # prompt before reboot, on all nodes
    'mosh', # like SSH
    'ncdu', # drill down per-folder disk usage
    $ntpd, # make sure time is synced
    'pciutils', # lspci
    'pcregrep', # for more a powerful grep
    'plocate', # for finding things faster
    'powertop', # power usage optimization tool
    'powerstat', # power usage measurement tool, see https://anarc.at/hardware/laptop/framework-12th-gen/
    'progress', # monitor long transfers
    'pv', # magic progress bar, used all the time
    'rename', # easier batch renames
    'reptyr', # reparent process
    'ripgrep', # just too fast to live without nowadays, minimal (runtime) deps
    'rsyslog', # debian default, but see also journald, syslog-ng
    'smartmontools', # check disks everywhere
    'sudo', # basic authentication jump, TODO: switch to doas?
    'tailspin', # for coloring logs
    'tmux', # see also profile::screen
    'time', # improved built-in time with resource usage
    'ttyrec', # part of the upgrade procedure
    'vim', # basic editor
    'yq', # pipe YAML into jq, see also dasel
    'zstd', # better compression, apparently used for initrds now
  ]:
    ensure => present,
  }
  package { [
    'apt-forktracer', # replaced with plain apt list
    'apt-show-versions', # using apt-forktracer instead
    'ccze', # replaced by tailspin
    'discover', # use lshw instead
    'inetutils-ping', # ping, but with SUID, use iputils-ping instead
    'inetutils-telnet', # use nc -t
    'inetutils-traceroute', # SUID, use mtr instead
    'nfs-kernel-server', 'rpc-statd', 'rpcbind', # hardening, somehow installed by default by d-i
    'ntp', # use openntpd instead
    'iotop', # replaced by more featureful and better maintained iotop-c
    'traceroute', # use mtr instead
    'tuptime', # uptime recorder TODO: move to common?
  ]:
    ensure => purged,
  }
}
