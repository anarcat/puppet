# an email server
#
# much more needs to be configured here, namely Dovecot in particular
#
# but for now, this handles the hard part, which is to collect SSH
# keys
class profile::email::server {
  # code in common with the client
  include profile::email
  # TODO: import SSH keys. for now this doesn't work because <<| tag
  # == foo |>> fails in the redis backend
  package { [
    'alot',
    'alpine',
    'bogofilter', # TODO: unused?
    'clamav-freshclam',
    'crm114', # TODO: unused?
    'dovecot-imapd',
    'dovecot-managesieved',
    'opendkim',
    'opendkim-tools',
    'opendmarc',
    'postfix-pcre',
    'postgrey',
    'pyzor', # TODO: unused?
    'razor', # TODO: unused?
    'spamassassin-heatu',
    'spampd',
    'vacation', # TODO: unused?
  ]:
    ensure => installed,
  }
}
