# email client configuration, makes sure that relevant packages are
# installed. most config is handled by dotfiles checked out from git.
class profile::email::client {
  include profile::email
  package { [
    'isync',
    'muttprint',
  ]:
    ensure => present,
  }
}
