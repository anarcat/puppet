# offsite backup server
class profile::backups::offsite {
  package { 'git-annex':
    ensure => present,
  }
  ensure_packages(['borgbackup'])
}
