# basic doas configuration
#
# decent alternative, not reviewed
# https://github.com/cirrax/puppet-doas
class profile::doas {
  package { 'doas': }
  -> file { '/etc/doas.conf':
    ensure  => present,
    mode    => '0400',
    content => @(EOF),
    permit persist :sudo
    EOF
  }
}
