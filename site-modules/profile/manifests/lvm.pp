# typical RAID + LUKS + LVM + ext4 stack
#
# compare with profile::zfs
class profile::lvm {
  # SMART monitoring is already in smartmontools in base
  #
  # TODO: email? what else is there here?
  package { [
    'cryptsetup-initramfs',
    'dropbear-initramfs',
    'mdadm',
  ]:
  }
}
