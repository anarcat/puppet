# tools i use to write articles, technical reviews, etc
class profile::author {
  include profile::emacs
  package { [
    'dict',
    # all dictionnaries
    # not sure
    #'dict-devil',
    'dict-elements',
    'dict-foldoc',
    # not sure why those are not on angela
    #'dict-freedict-eng-fra',
    #'dict-freedict-eng-spa',
    #'dict-freedict-fra-eng',
    #'dict-freedict-spa-eng',
    #'dict-gazetteer2k',
    'dict-gcide',
    'dict-jargon',
    'dict-vera',
    'dict-wn',
    'dictd',
    # never used
    #'dictionary-el',
    #'epubcheck',
    'gv',
    'latexmk', 'texlive-fonts-extra', # slides for work
    'libtext-multimarkdown-perl', # for the `multimarkdown` binary
    'markdownlint', # for the "mdl" linter
    'pandoc',
    'python3-md-toc', # table of contents for markdown files, md_toc bin
    'sigil', # edit and fix up ePUB files
    'texlive-latex-base',
    'texlive-latex-recommended',
    'texlive-latex-extra',
    'texlive-luatex',
  ]:
    ensure => present,
  }
  package { [
    # removed from Debian, https://bugs.debian.org/964990
    'dict-bouvier',
    # https://bugs.debian.org/964991
    'dict-moby-thesaurus',
  ]:
    ensure => 'purged',
  }
}
