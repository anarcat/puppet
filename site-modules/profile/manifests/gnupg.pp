# gnupg configuration, including yubikey hacks
class profile::gnupg(
  Boolean $gui = true,
) {
  ensure_packages([
    'gnupg',
    'gpg-agent',
    'pcscd',
    'scdaemon',
  ])
  package { 'gnupg-agent':
    ensure => purged,
  }

  $gui_ensure = $gui ? {
      true => 'present',
      false => 'purged',
  }
  package { [
    'pinentry-qt',
    'yubikey-personalization',
    'yubikey-manager',
    'yubioath-desktop',
  ]:
    ensure => $gui_ensure,
  }
  if $gui {
    file { '/etc/alternatives/pinentry-x11':
      ensure => link,
      target => '/usr/bin/pinentry-qt',
    }
    file { '/etc/alternatives/pinentry':
      ensure => link,
      target => '/usr/bin/pinentry-qt',
    }
  }
  # pinentry-curses is a requirement of gnupg-agent so implicit

  # removed from debian
  package { [
    'wotsap',
  ]:
    ensure => 'purged',
  }
}
