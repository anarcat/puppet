# install docker from Debian
class profile::docker(
  Enum['present','absent'] $ensure = 'present',
) {
  include nftables
  include nftables::rules::docker_ce
  class { 'docker':
    ensure                      => $ensure,
    use_upstream_package_source => false,
    docker_ce_package_name      => 'docker.io',
    docker_ce_start_command     => '',
    # the module template puts dockerd in /usr/bin/ instead of
    # /usr/sbin/, which is where the docker.io Debian package (more
    # logically, IMHO) put it. just skip the override altogether to
    # bypass that.
    service_overrides_template  => false,
    # nftables takes care of the docker firewall, above
    iptables                    => false,
  }
  # prune unused/untagged images
  cron { 'docker-prune-images':
    ensure  => $ensure,
    command => 'docker image prune -f | grep -v "Total reclaimed space: 0B"',
    user    => root,
    hour    => 3,
    minute  => 33,
    weekday => 1,
  }
  package { [
    # class docker::compose does a curl | bash, ignore
    'docker-compose',
    'runc',
    'libseccomp2',
  ]:
    ensure  => $ensure,
  }
}
