# manage my bookmarks, brutally
class profile::firefox {
  $trixie_or_later = $facts['os']['release']['major'] ? {
    'trixie/sid' => true,
    '13' => true,
    '12' => false,
  }
  # for vertical tabs, shipped in 136, will be ESR in 140, after
  # trixie's freeze, most likely
  if $trixie_or_later {
    package { 'firefox':
      ensure => installed,
    }
    profile::package::pin { [ 'firefox', 'libnss3' ]: }
    package { 'firefox-esr':
      ensure => purged,
    }
  } else {
    package { 'firefox':
      ensure => purged,
    }
    package { 'firefox-esr':
      ensure => installed,
    }
  }
  package { [
    'webext-browserpass',
    'webext-ublock-origin-firefox',
    'webext-ublock-origin-chromium',
  ]:
  }
  file { '/etc/firefox/':
    ensure => 'directory',
  }
  file { '/etc/firefox/policies/':
    ensure => 'directory',
  }
  file { '/etc/firefox/policies/policies.json':
    source => 'puppet:///modules/profile/firefox/policies.json',
  }

  package { [
    # seems like neither Firefox or Chromium need this, at least in bullesye
    'libu2f-host0',
    # not working
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919557
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=916594
    'webext-umatrix',
  ]:
    ensure => purged,
  }
  sysctl::config { 'userns':
    key     => 'kernel.unprivileged_userns_clone',
    value   => '1',
    comment => 'allow Firefox to create a better sandbox, see https://wiki.mozilla.org/Security/Sandbox',
  }
}
