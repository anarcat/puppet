# graphic design and photography tools.
class profile::graphics {
  package { [
    'darktable',
    'dia',
    # image viewers, also considered
    # nomacs (removed in Debian bullseye)
    # eog (part of gimp)
    # ... and many more:
    # https://anarc.at/blog/2020-09-30-presentation-tools/#other-options
    'feh',
    'geeqie',
    'gimp',
    'inkscape',
    'inkscape-open-symbols', # clipart to draw diagrams
    'kdenlive',
    'mpv',
    # another alternative: Kooha https://github.com/SeaDve/Kooha
    # https://bugs.debian.org/1055956
    #
    # i've also used simplescreenrecorder for audio recording, not
    # currently supported by peek:
    # https://github.com/phw/peek/issues/105
    'peek',
    'rapid-photo-downloader',
    'sane',
    'vlc',
    'xsane',
    'yt-dlp',
  ]:
    ensure => present,
  }
  package { [
    'colorhug-client',
    'dispcalgui',
    'siril',
    'xawtv-tools',
  ]:
    ensure => purged,
  }
  # transition package
  package { 'youtube-dl':
    ensure => 'purged',
  }
  # replaced by peek
  package { [
    'gtk-recordmydesktop',
    'recordmydesktop',
  ]:
    ensure => 'purged',
  }
}
