class profile::bind(
  Boolean $dnssec = true,
  ) {
  class { 'bind':
    dnssec        => $dnssec,
    include_local => true,
  }
  bind::logging::category { 'default':
    channels => ['default_syslog'],
  }
  # restore debian package default
  file { '/etc/bind/named.conf.local':
    content => @(EOF),
    //
    // Do any local configuration here
    //

    // Consider adding the 1918 zones here, if they are not used in your
    // organization
    //include "/etc/bind/zones.rfc1918";
    | EOF
    notify  => Service['bind'],
  }
}
