# ensures host automatically restarts processes after upgrades
#
# @param ensure if the package and its configuration should be
#               installed or removed
#
# @param restart if packages should be automatically restarted or not
class profile::needrestart(
  Enum['present','absent'] $ensure = 'present',
  Enum['list', 'interactive', 'automatically'] $restart = 'automatically',
) {
  $restart_letter = $restart ? {
    'list' => 'l',
    'interactive' => 'i',
    'automatically' => 'a',
  }
  class {
    'needrestart':
      package_ensure => $ensure,
      configs        => {
        'restart'   => $restart_letter,
        # override blacklist to allow detection of dhclient, see
        # https://github.com/liske/needrestart/issues/225
        'blacklist' => [
          # ignore sudo (not a daemon)
          'qr(^/usr/bin/sudo(\.dpkg-new)?$)',
          # ignore apt-get (Debian Bug#784237)
          'qr(^/usr/bin/apt-get(\.dpkg-new)?$)',
        ],
      }
  }
  file { '/etc/needrestart/conf.d/safe-overrides.conf':
    content => @(EOF),
    # file managed by Puppet, local changes will be lost
    #
    # do not restart ifup because of dhclient, it may break. should be
    # upstream, see https://github.com/liske/needrestart/issues/225
    $nrconf{override_rc}{qr(^ifup@.+\.service)} = 0;
    | EOF
    require => [File['/etc/needrestart/conf.d/'],Class['needrestart::install']],
  }
}
