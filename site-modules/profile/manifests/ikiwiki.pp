# basic ikiwiki configuration
#
# TODO: lacks actual wiki setup and deployments
class profile::ikiwiki {
  package { [
    'bibtex2html',
    'ikiwiki',
    'ikiwiki-hosting-web',
    'libtext-bibtex-perl',
    'xapian-omega',
    'xapian-tools',
  ]:
    ensure => installed,
  }
}
