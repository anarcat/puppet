# ham radio tools
class profile::radio {
  package { [
    'ax25-apps',
    'chirp',
    'direwolf',
    'fldigi',
    'gnuradio',
    'gpredict',
    'gqrx-sdr',
    'horst', # monitor WiFi signals
    'multimon',
    'soundmodem',
    'splat',
    'xastir',
  ]:
    ensure => present,
  }
}
