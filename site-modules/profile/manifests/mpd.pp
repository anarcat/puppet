# configure a typical mpd client
class profile::mpd(
  Boolean $server = false,
) {
  package { 'mpd': }
  # disable the automatic startup of mpd.service for *all* user
  # sessions. users interested in this should run `systemctl enable
  # mpd.service`
  #
  # this was filed as bug https://bugs.debian.org/1017921
  -> file { '/etc/systemd/user/default.target.wants/mpd.service':
    ensure  => absent,
  }
  if $server {
    $password = trocla("profile::mpd::${::hostname}", 'plain')
    file { '/etc/mpd.conf':
      source => 'puppet:///modules/profile/mpd.conf',
      notify => Service['mpd'],
    }
    -> file { '/etc/mpd.conf.d/':
      ensure  => 'directory',
      purge   => true,
      recurse => true,
      force   => true,
      notify  => Service['mpd'],
    }
    -> file { '/etc/mpd.conf.d/server.conf':
      mode    => '0440',
      owner   => 'root',
      # ideally, this would be the mpd group, but that doesn't
      # exist. mpd is part of the audio group however, so this is a good
      # compromise
      group   => 'audio',
      content => @("EOF")
      bind_to_address "any"
      zeroconf_enabled               "yes"
      default_permissions    "read"
      password "${password}@read,add,control,admin"
      | EOF
    }
    -> service { 'mpd': }
  } else {
    file { '/etc/mpd.conf.d/server.conf':
      ensure => absent,
    }
  }
}
