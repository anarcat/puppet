# setup debsecan on machines
#
# this is mostly to follow security upgrades from unstable in testing
class profile::debsecan(
  Enum['present','absent'] $ensure = 'present',
) {
  package { 'debsecan':
    ensure => $ensure,
  }
  if $ensure == 'present' {
    file_line { 'disable_debsecan_mails':
      path  => '/etc/default/debsecan',
      line  => 'REPORT=false',
      match => '^REPORT=.*',
    }
    file { '/etc/apt/preferences.d/debsecan.pref':
      ensure  => link,
      target  => '/var/lib/debsecan/apt_preferences',
      require => File['/var/lib/debsecan/apt_preferences'],
    }
  } else {
    file { '/etc/apt/preferences.d/debsecan.pref':
      ensure => absent,
    }
  }
  file { '/usr/sbin/debsecan-apt-priority':
    ensure => $ensure,
    source => 'puppet:///modules/profile/debsecan-apt-priority',
    mode   => '0555',
  }
  file { '/etc/apt/apt.conf.d/99debsecan':
    ensure  => $ensure,
    content => @(EOF),
    APT::Update::Pre-Invoke { "/usr/sbin/debsecan-apt-priority"; };
    EOF
  }
  file { '/var/lib/debsecan/apt_preferences':
    ensure => $ensure,
  }
}
