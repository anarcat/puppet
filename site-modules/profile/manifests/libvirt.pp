# configure a virtual machine host with libvirt
class profile::libvirt {
  include libvirt
  class { 'nftables::rules::qemu':
    masquerade => false,
  }
  package {[
    'bridge-utils', # for making bridges, often required for VMs
    'qemu-utils', # qemu-img
    'virtinst', # for virt-install
  ]:
    ensure => present,
  }
  # allow the libvirt group access to the daemon, without
  # authentication
  libvirtd_conf {
    'unix_sock_group': value => 'libvirt';
    'auth_unix_rw':    value => 'none';
  }
}
