# This is mp3, ogg tools, audio recording and playing software, video playing screencasting software, etc.
class profile::audio {
  package { [
    'alsa-utils', # alsamixer, speaker-test
    'ardour',
    'audacious', # standalone music player, alternative: deadbeef, not in debian https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=576975
    'audacity',
    'beets',
    'exfalso',
    'gmpc', # unmaintained, see ymuse below for a replacement
    'gmpc-plugins',
    'mediainfo',
    'mpc',
    'mpdtoys',
    'sox',
    # who writes CDs anyways
    #'wodim',
    'ymuse', # MPD music client
  ]:
    ensure => present,
  }

  # not packaged in Debian, see https://bugs.debian.org/1034360
  flatpak { 'io.github.dweymouth.supersonic':
    ensure => present,
    remote => 'flathub',
  }
  file { '/usr/local/bin/supersonic':
    ensure  => 'link',
    target  => '/var/lib/flatpak/app/io.github.dweymouth.supersonic/current/active/export/bin/io.github.dweymouth.supersonic',
    require => Flatpak['io.github.dweymouth.supersonic'],
  }

  # too slow, flaky, replaced with the above Supersonic
  package { 'sublime-music':
    ensure => purged,
  }
  # unmaintained upstream
  package { 'cantata':
    ensure => purged,
  }
}
