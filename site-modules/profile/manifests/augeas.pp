# overrides for augeas
class profile::augeas {
  # used by the augeas puppet module
  #
  # this is necessary to *bootstrap* puppet, but it *must* be
  # present for the module to work at all, so keep it here just in
  # case we can bootstrap here...
  package { 'augeas-tools':
    ensure => present,
  }
  class { 'augeas':
    # hack around https://github.com/camptocamp/puppet-augeas/issues/65
    lens_dir => '/usr/share/augeas/lenses/',
  }
}
