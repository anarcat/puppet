# install puppetboard in a docker container
class profile::puppetboard {
  include profile::docker
  $secret_key = stdlib::fqdn_rand_string(32, undef, 'puppetboard-secret-key')
  file { '/etc/docker/puppetboard':
    ensure => directory,
  }
  -> file { '/etc/docker/puppetboard/docker-compose.yml':
    ensure => present,
    source => 'puppet:///modules/profile/compose-puppetboard.yml',
  }
  -> file { '/etc/docker/puppetboard/secret-key.env':
    content => @("EOF"),
    SECRET_KEY=${secret_key}
    | EOF
  }
  # this will start the container(s)
  -> docker_compose { 'puppetboard':
    ensure        => present,
    compose_files => ['/etc/docker/puppetboard/docker-compose.yml'],
  }
  -> cron { 'update-puppetboard':
      command => 'docker-compose -f /etc/docker/puppetboard/docker-compose.yml -p puppetboard pull && docker-compose -f /etc/docker/puppetboard/docker-compose.yml -p puppetboard up -d', # lint:ignore:140chars
      user    => 'root',
      hour    => 5,
      minute  => 35,
      weekday => 3,  # wednesday
    }
}
