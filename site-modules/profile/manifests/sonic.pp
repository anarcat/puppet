# setup a subsonic-compatible server
class profile::sonic(
  Enum['present','absent'] $ensure = 'present',
) {
  include profile::docker
  if $ensure == 'present' {
    docker_volume { 'navidrome_navidrome-data':
      ensure => $ensure,
    }
    # hack to make the volume readable to Docker
    -> file { '/var/lib/docker/volumes/navidrome_navidrome-data/_data/':
      ensure => directory,
      # those are defined in the docker compose file below, compose-navidrome.yml
      owner  => 10000,
      group  => 10000,
      mode   => '0750',
    }
    # TODO: create that user, maybe? that would at least ensure we
    # don't have multiple containers running at the same user...

    -> file { '/etc/docker/navidrome':
      ensure => directory,
    }
    # TODO: consider podman instead of docker compose, for future
    # interop with (say) kubernetes (at least that's the argument
    # podman is making for not supporting compose, last i checked)

    # the docker compose file
    -> file { '/etc/docker/navidrome/docker-compose.yml':
      ensure => $ensure,
      source => 'puppet:///modules/profile/compose-navidrome.yml',
    }
    # this will start the container(s)
    -> docker_compose { 'navidrome':
      ensure        => $ensure,
      compose_files => ['/etc/docker/navidrome/docker-compose.yml'],
    }
    # another option for automated upgrades: https://containrrr.dev/watchtower/
    cron { 'update-navidrome':
      command => 'docker-compose -f /etc/docker/navidrome/docker-compose.yml -p navidrome pull && docker-compose -f /etc/docker/navidrome/docker-compose.yml -p navidrome up -d', # lint:ignore:140chars
      user    => 'root',
      hour    => 5,
      minute  => 35,
      weekday => 2,  # tuesday
    }
  } else {
    # reverse order of the above, without the _data hack
    docker_compose { 'navidrome':
      # note that this will fail the second time this manifest is ran,
      # because the YML file will be gone
      ensure        => $ensure,
      compose_files => ['/etc/docker/navidrome/docker-compose.yml'],
    }
    -> file { '/etc/docker/navidrome/docker-compose.yml':
      ensure => $ensure,
      source => 'puppet:///modules/profile/compose-navidrome.yml',
    }
    -> docker_volume { 'navidrome_navidrome-data':
      ensure => $ensure,
    }
  }
}
