anarc.at Puppet code
====================

This is the repository holding code for my personal Puppet
infrastructure. Far from being fully converted, only some bits are
covered by Puppet.

All the code here is public, but might not always be fully functional
as it's possible some secrets are not shipped with the repository.

Third party -- including secret -- repositories should be documented
in the `Puppetfile`.

Installation
============

## Bootstrapping a puppet master

This code needs to be cloned in `/etc/puppet/code`, then the `puppet`
and `librarian-puppet` packages need to be installed. Then:

    librarian-puppet install

Should deploy the missing code. Check that the `.lock` file is
unchanged. Then run the code relevant to the Puppet server with `puppet
apply` and watch the fireworks.

XXX: this does not work. The Foreman puppet module (arguably
correctly) assumes `puppetserver` exists, which is not the case in
Debian. For now we just use the `puppetmaster` package and assume it
self-configures.

## Adding nodes

On nodes:

    puppet agent --test --waitforcert

Copy-paste the checksum, then on the server:

    puppet ca list | grep "$CHECKSUM"

If the node is in the output, run:

    puppet ca sign "$NODE"

TODO: improve bootstrapping to avoid race condition. Replace use of
deprecated `puppet-ca` by whatever replaces it.

TODO: Seems like the trend is to setup a [control-repo](https://puppet.com/docs/pe/latest/control_repo.html) using [this
template](https://github.com/puppetlabs/control-repo), or at least configure the [environment](https://puppet.com/docs/puppet/latest/environments_about.html) somehow. For
now, I hacked my way around this by hard-coding `/etc/puppet/code` as
the `environmentpath` and cloned it there, but that's far from ideal,
as things get confused quickly. We also do not manage Hiera that way
at all.

Local tests
-----------

You might be able to run the manifest locally with the `./run` command
if you're on the right node. Be careful to have an up to date
`modules/` directory.

Alternatives
------------

This used to be an [Ansible][] [YAML file][] but I never really used
Ansible, in the end. Ansible always felt awkward to me: it seems like
an overgrown shell script, written in this weird mix of YAML and Jinja
templates. It is slow. Writing modules is undocumented. And the code
of the modules I found (e.g. [the git module][]) were scary to look
at. (And yes, that's how you're supposed to learn how to write a
module, apparently, which explains a lot.)

Besides, I do not have remote access to all my machines, so I would
have needed some hacks to have the remotes talk to the server somehow
or do port forwarding or something. Puppet, on the other hand, has
agents checking in on the central server which bypasses firewall
issues completely.

[the git module]: https://github.com/ansible/ansible-modules-core/blob/devel/source_control/git.py
[Ansible]: https://www.ansible.com/
[YAML file]: http://source.anarcat.wiki.orangeseeds.org/?p=source.git;a=blob;f=software/packages.yml;h=a62f7728b657afb69e8f39f1fd9db5ad78b1f046;hb=9754a0c94918f98f00de2724324b59fd5ff64f14

I also considered using [propeller][] because I like its design and
spirit, but I was afraid I would need to reinvent too much. Besides:
as much as I like the language, I can never quite get the hang out of
Haskell, which is a requirement to write modules. In contrast, you can
write Puppet modules in the Puppet DSL, which I have become familiar
enough with. And worst case, you fallback to Ruby which is not that
bad...

[propeller]: https://propellor.branchable.com/

I have considered [Salt][] a while back, but my experience working on
it as part of the LTS security team (specifically in
[CVE-2017-7893][]) made me very uncomfortable about trusting it with
administrative access on my entire architecture. And [recent
vulnerabilities][] (specifically [CVE-2020-11651][] and
[CVE-2020-11652][], remote code execution!) have confirmed by worst
fears so I actually advise everyone to stay away from this product for
now.

[CVE-2020-11651]: https://security-tracker.debian.org/tracker/CVE-2020-11652
[CVE-2020-11652]: https://security-tracker.debian.org/tracker/CVE-2020-11652
[recent vulnerabilities]: https://threatpost.com/salt-bugs-full-rce-root-cloud-servers/155383/
[CVE-2017-7893]: https://security-tracker.debian.org/tracker/CVE-2017-7893
[Salt]: https://www.saltstack.com/

[Chef][] is also an interesting alternative, especially with their
approach of not requiring a custom DSL to write manifests, instead
relying on a Ruby-based DSL. But it never got the traction required
for me to make the switch. And although they [backtracked on that
position][], I definitely did not appreciate their [response to
criticism of them working with ICE][] back in 2019.

[response to criticism of them working with ICE]: https://blog.chef.io/a-personal-message-from-the-cto/
[backtracked on that position]: https://blog.chef.io/an-important-update-from-chef/
[Chef]: https://en.wikipedia.org/wiki/Chef_(software)

In the end, I use Puppet at work and I have been using it for over a
decade at this point, so it seems more reasonable to use it for
this. It would be such an undertaking to not only learn a new system,
but also (and especially) convert existing ones that it doesn't seem
worth it at all. It could even serve as a staging environment until we
figure out how to deploy that at work...
