# This is a Puppetfile:
#
# https://puppet.com/docs/pe/latest/puppetfile.html
#
# it details where all non-local repositories are. otherwise it is a
# monorepo of all my glue code around forge and third-party modules.

# To add a module here, either add the git remote and the desired
# commit (*not* the symbolic branch name: we want to audit the code)
# or checksum the release from the forge. To do the latter, visit the
# download link on the forge project and pipe it into curl. For
# example, to checksum duritong/sysctl 0.0.12, I used:
#
#     curl https://forge.puppet.com/v3/files/duritong-sysctl-0.0.12.tar.gz | sha256sum - 
#
# Then run `g10k -puppetfile -moduledir modules` to test the
# configuration locally. This should be also ran on push.

# And yes, this means two commits if you work on one of those
# modules. Typically, the way around that is through unit tests or
# examples/ files that you run with Puppet apply, or at least with
# environments to avoid too many YOLO commits on the main branch.

forge "https://forgeapi.puppetlabs.com"

# g10k will pick the first directory it finds, which, in our case, is
# site-modules, and install files there. work around that problem by
# explicitly setting the default
#
# this is disabled to keep librarian-puppet functional. Just call g10k
# with the -moduledir argument as directed above.
#moduledir 'modules'

mod 'saz/dnsmasq', '1.5.0',
    :sha256sum => 'f02f88c3ccf93b1838f4ee1b683d30140820e52d73fb3d5281e8c88ea99521d0'

# manage my irssi sessions
mod 'anarcat/dtach',
    :git => 'https://gitlab.com/anarcat/puppet-dtach',
    :commit => '99f84a1f1b6e0f4f140fc6659cd69cc669386e10'
    # :branch => master

mod 'puppetlabs-docker', '3.14.0',
    :sha256sum => 'd67dd546c9ee67c2742ce8e97204f208ee0aa26354ee25f882e6a43bf0a7d284'

mod 'inkblot/bind',
    # forked off of jcharaoui's fork, one pending MR:
    # https://github.com/jcharaoui/puppet-bind/pull/3
    :git => 'https://github.com/anarcat/puppet-bind-inkblot',
    :commit => 'd9dfaa543f66549dd91fe4f0f14fbabc96f70f37'

# https://github.com/brwyatt/puppet-flatpak/pull/10
mod 'brwyatt/flatpak',
    :git => 'https://github.com/anarcat/puppet-flatpak',
    :commit => '4e72a758dda0a46677e7ce4c406551f13054dc5d'
    # :branch => 'optional-apt'

mod 'anarcat/icecast2',
    :git => 'https://gitlab.com/anarcat/puppet-icecast2/',
    :commit => '834babefaba182b96c54fc08b8a2cf4e090e5713'
    # :branch => master

# 4.3.4 + bullseye support
mod 'cirrax/libvirt',
    :git => 'https://github.com/cirrax/puppet-libvirt/',
    :commit => 'ef5ecb543205135fc825c4b82013952b35b6fe34'

# puppet 8 support
mod 'hetzner/needrestart',
    :git => 'https://github.com/xneelo/hetzner-needrestart.git',
    :commit => '2eec1bbccf007109494d8b66652ea75091fca15a'

mod 'puppet/nftables', '2.6.0',
    :sha256sum => '53bcbd308220cfbcef298a12f736656c594179d0e4035564c9564f4e721dfff6'

mod 'puppet/nginx',
    :git => 'https://github.com/voxpupuli/puppet-nginx.git',
    # audited at Tor
    :commit => '63b75aa9050b5751ae354a6cf1ecd0183668f999'

# to manage my ssh keys
# I also considered:
#
# https://github.com/ghoneycutt/puppet-module-ssh
# https://github.com/saz/puppet-ssh
#
# but neither of those exported the user's keys to other hosts, nor
# did it support SSH key generation.
#
# authorized_keys_collect MR pending on
# https://gitlab.com/shared-puppet-modules-group/sshd/
# also missing trixie MR
mod 'smash/sshd',
    :git => 'https://gitlab.com/anarcat/sshd',
    :commit => 'b47e8a2099d9a49632a915860600b378b5a62b02'
    # :branch => master

mod 'puppetlabs-sshkeys_core', '2.3.0',
    :sha256sum => '03b1ae21fff8483f9b04f68b5c26b997f28dc0264de135ce1f3595d622d9fd71'

# this does not work in Debian because of:
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=950182
# ... among other things, probably.
#
# try to configure a puppetmaster, and agents
#
# TODO: upgrade to 14.2.1 or later (but < 16.0 which drops support for
# Puppet 5) for https://github.com/theforeman/puppet-puppet/pull/765
mod 'theforeman/puppet', '14.0.0',
    :sha256sum => 'a68dd8dda60fc8604b60840713b56f8c911475eaedc74757b09ac9c8096a6f75'
# requirement of the above
#mod 'puppetlabs/puppetdb'

# tested this as well...
#mod 'puppetlabs/puppet_agent'
# but that module that doesn't work in Puppet 5 (non-PE at least):
# Error while evaluating a Function Call, The puppet_agent module does
# not support pre-Puppet 4 upgrades. (file:
# /home/anarcat/src/puppet/modules/puppet_agent/manifests/init.pp,
# line: 120, column: 5) on node emma.anarc.at
#
# besides it doesn't seem to support running as a cron job

# to manage mail servers, obviously
mod 'puppet/postfix', '5.0.0',
    :sha256sum => '072eb438767eb55b209a51e84b99b0394c86afbad5596140c98f89a2af224c2d'

mod 'anarcat/sbuild',
    :git => 'https://gitlab.com/anarcat/puppet-sbuild.git',
    :commit => 'd2560448f53491197a22601c4d8ca630ddc8ae41'

mod 'smash/tor',
    :git => 'https://gitlab.com/shared-puppet-modules-group/tor.git',
    :commit => 'ae9c9d54ce2d03afda08a319911c9bfa473e303f' # 4.0.0

mod 'CraigWatson1987/transmission',
    :git => 'https://github.com/craigwatson/puppet-transmission.git',
    :commit => '813340130e9c8c6a1a818c74af87e10c503c4ea0'

mod 'duritong/trocla',
    :git => 'https://github.com/duritong/puppet-trocla.git',
    # current master
    :commit => '2453cfdc988dfcb0cd4f390bb391c85904c42467'

mod 'puppet/unattended_upgrades', '8.1.0',
    :sha256sum => 'd695630e5004641098a5ab8967b442d933ac36afcc9a1d964d7a08b5fd44ac4e'

# dependencies
mod 'puppetlabs/apt', '9.4.0',
    # TOFU on the checksum, version audited at Tor, code compared and matches
    :sha256sum => '58f29529768d6b28d7b3a641a33a955e9347b5d987597946e5e38aac7a7dc4f2'
mod 'camptocamp-systemd', '2.12.0', :sha256sum => '7be455a364b2b368f50c66e74a2e07ec0c8e8c3ced00ddbb258931f94b45619f'
mod 'puppet-extlib', '7.0.0', :sha256sum => 'e9c75bdf621a1645814d2025c7cb24df21c66c26ed64862a0c332205ab742df4'
mod 'puppetlabs-concat', '6.2.0', :sha256sum => 'cb6890945dd458077c5a7b2e181bfbf8ef5c8cf20c9a2977b8ca192e0125871b'
mod 'puppetlabs-inifile', '4.2.0', :sha256sum => '07c53c25c3ed198afea0b1ade0cf0f7f0df4de9f4345b334f21389713202b514'
mod 'puppetlabs-stdlib', '9.4.1', :sha256sum => '2498431032871bd30e600d515e7b4f412962e409fbf60333104295a62d79cd80'
mod 'puppetlabs-translate', '2.2.0', :sha256sum => '18e58f7b9894d258ee153aa800437770b9af931025ff640ad762665c1ff7b75d'
mod 'puppetlabs-augeas_core', '1.0.5', :sha256sum => 'f94123c7aa1fef564bb1ca82271f2d4e36cb6b82bacb0833154d5dab73ac7a34'
mod 'puppet-augeas', '2.0.0', :sha256sum => 'af1ef259930698ab373f51d074256d1ad0f322e119885fa49f888a122e6379bd'
mod 'puppet-alternatives', '3.0.0', :sha256sum => '488e0573d0fa6f8475f979001a72ebc0fef0b9015c753649164c173630851149'
mod 'puppetlabs-mailalias_core', '1.0.6', :sha256sum => '69f990a8027360af2cf819a94ff0e954bfc03ae1c4fe295acac1eafaa511b066'
