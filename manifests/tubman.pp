node 'tubman.anarc.at' {
  include profile::zfs
  include profile::network
  include profile::postfix::satellite
  include profile::systemd
  include profile::backups::offsite
  include profile::tor
}
