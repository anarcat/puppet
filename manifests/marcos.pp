node 'marcos.anarc.at' {
  include role::server::web
  include role::server::puppet
  include role::server::shell
  include profile::etckeeper
  include profile::icecast2
  include profile::webmail::alps
  # TODO: move to a radio role?
  include profile::sonic
  class { 'profile::mpd':
    server => true,
  }
  include profile::ikiwiki
  include profile::libvirt
  include profile::lvm
  include profile::metalfinder
  class { 'profile::unbound':
    manage_networkmanager => false,
  }
  include profile::ups
  class { 'profile::seedbox':
    # max is 25mbps down, which is ~3MB/s, cap to 2MB/s
    speed_limit_down => 2000, # KiB/s
    # this location has ~6Mbit/s up, which is 750KB/s, cap at 600
    speed_limit_up   => 600,
  }

  ['ursula', 'imac-de-simonvv'].each|$user| {
    file { "/etc/ssh/sshd_config.d/${user}.conf":
      notify  => Service[sshd],
      content => @("EOF"),
      Match User ${user}
         ChrootDirectory /srv
         ForceCommand internal-sftp
         AllowTcpForwarding no
         X11Forwarding no
      | EOF
    }
  }

  # temporary, while we build our firewall rules, just to block
  # certain key services
  include profile::nftables::in::all
  nftables::rule { 'default_in-puppetdb':
      content => 'tcp dport {8080,8082} return',
  }

}
