node 'curie.anarc.at' {
  # TODO: this belongs in hiera, but it's really annoying to move
  # because we need some iterator to create bridges and networks,
  # probably in profile::network, argh.
  profile::network::bridge { 'br0':
    address => '192.168.0.66/24',
    gateway => '192.168.0.1',
  }
  systemd::network { 'he-tunnel.netdev':
    ensure => absent,
  }
  systemd::network { 'he-tunnel.network':
    ensure => absent,
  }
  class { 'profile::zfs':
    manage_sanoid    => true,
    pools            => ['rpool', 'bpool'],
    sanoid_extra_cfg => @(EOF),
    # snapshot home more frequently
    [rpool/home]
    frequently = 3
    # 15 minutes is the frequency of the timer anyways, so we cannot
    # get more granular
    frequent_period = 15

    # reduce the snapshot frequency on boot pool, as the kernels fill
    # up even the 1GB boot partition after a few months, especially
    # when running testing
    [bpool]
    hourly = 0
    daily = 7
    monthly = 2
    | EOF
  }
  include role::workstation
  # curie has a bad CMOS battery
  include profile::fake_hwclock
}
