# we use one manifest file per node
#
# we could have used Hiera, but at this point our infra is so small (3
# nodes?) that it's definitely not worth it
#
# also, we use puppet apply for now, which, as far as i know, won't
# talk to hiera, so do not bother.
#
# https://puppet.com/docs/puppet/latest/services_apply.html

# files managed by puppet should be readonly to make it obvious we
# should not change them by hand
File {
    owner   => root,
    group   => root,
    mode    => '444',
    ensure  => file,
}

# those are classes included absolutely everywhere
#
# bootstrap the puppet agent.
include profile::puppet
# a set of packages to install absolutely everywhere
include profile::base
# basic shell configuration
include profile::bash
# enable unattended-upgrades everywhere
include profile::unattended_upgrades
# and automatically restart things afterwards
include profile::needrestart
# configure sshd mostly the same way everywhere, variations in hiera
include profile::sshd

node default {
  # include base class for node's role
  # this class should include a series of profiles
  if $role {
    include "role::${role}"
  }
}
